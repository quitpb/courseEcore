# Modelling information in course web page
 I've made two xmi-models, Course.xmi and Department.Xmi. These two models represent a course instance with or without department that are responsible for the course. 

## The model

**Department**: container onject for Employee, StudyProgram, Course

**Course**: container object for TimeTable,RelationToCourse, Evaluation, CourseWork, CourseInstances, Staff

**Employee**: container for Lecturer, CourseCordinator

**TimeTable**: container for Slot


**Staff**: container for Employee

**Constraints** atleastOneCourseCordinator, maxHundredpoints, scheduleHoursMinCourseAndLab  

atleastOneCourseCordinator: cannot be null

maxHundredpoints: sum of all points from the 
evaluation cannot exceed 100 points.

scheduleHoursMinCourseAndLab : sum of all slot.hours must atleast be the same as courwework.lectureHours + courseWork.labHours
