/**
 */
package coursePage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Related Course</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see coursePage.CoursePagePackage#getRelatedCourse()
 * @model
 * @generated
 */
public enum RelatedCourse implements Enumerator {
	/**
	 * The '<em><b>SIF8060</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIF8060_VALUE
	 * @generated
	 * @ordered
	 */
	SIF8060(1, "SIF8060", "SIF8060"),

	/**
	 * The '<em><b>IT1104</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IT1104_VALUE
	 * @generated
	 * @ordered
	 */
	IT1104(2, "IT1104", "IT1104"),

	/**
	 * The '<em><b>SIF8005</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIF8005_VALUE
	 * @generated
	 * @ordered
	 */
	SIF8005(3, "SIF8005", "SIF8005"),

	/**
	 * The '<em><b>TDT4102</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TDT4102_VALUE
	 * @generated
	 * @ordered
	 */
	TDT4102(4, "TDT4102", "TDT4102");

	/**
	 * The '<em><b>SIF8060</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SIF8060</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIF8060
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIF8060_VALUE = 1;

	/**
	 * The '<em><b>IT1104</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IT1104</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IT1104
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int IT1104_VALUE = 2;

	/**
	 * The '<em><b>SIF8005</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SIF8005</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIF8005
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIF8005_VALUE = 3;

	/**
	 * The '<em><b>TDT4102</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TDT4102</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TDT4102
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TDT4102_VALUE = 4;

	/**
	 * An array of all the '<em><b>Related Course</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final RelatedCourse[] VALUES_ARRAY =
		new RelatedCourse[] {
			SIF8060,
			IT1104,
			SIF8005,
			TDT4102,
		};

	/**
	 * A public read-only list of all the '<em><b>Related Course</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<RelatedCourse> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Related Course</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RelatedCourse get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RelatedCourse result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Related Course</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RelatedCourse getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RelatedCourse result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Related Course</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RelatedCourse get(int value) {
		switch (value) {
			case SIF8060_VALUE: return SIF8060;
			case IT1104_VALUE: return IT1104;
			case SIF8005_VALUE: return SIF8005;
			case TDT4102_VALUE: return TDT4102;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private RelatedCourse(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //RelatedCourse
