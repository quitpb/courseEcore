/**
 */
package coursePage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Study Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.StudyProgram#getCode <em>Code</em>}</li>
 *   <li>{@link coursePage.StudyProgram#getCanSignUpFor <em>Can Sign Up For</em>}</li>
 *   <li>{@link coursePage.StudyProgram#getBelongsTo <em>Belongs To</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getStudyProgram()
 * @model
 * @generated
 */
public interface StudyProgram extends EObject {

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * The literals are from the enumeration {@link coursePage.StudyCode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see coursePage.StudyCode
	 * @see #setCode(StudyCode)
	 * @see coursePage.CoursePagePackage#getStudyProgram_Code()
	 * @model
	 * @generated
	 */
	StudyCode getCode();

	/**
	 * Sets the value of the '{@link coursePage.StudyProgram#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see coursePage.StudyCode
	 * @see #getCode()
	 * @generated
	 */
	void setCode(StudyCode value);

	/**
	 * Returns the value of the '<em><b>Can Sign Up For</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link coursePage.Course#getPlannedFor <em>Planned For</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Can Sign Up For</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Can Sign Up For</em>' reference.
	 * @see #setCanSignUpFor(Course)
	 * @see coursePage.CoursePagePackage#getStudyProgram_CanSignUpFor()
	 * @see coursePage.Course#getPlannedFor
	 * @model opposite="plannedFor"
	 * @generated
	 */
	Course getCanSignUpFor();

	/**
	 * Sets the value of the '{@link coursePage.StudyProgram#getCanSignUpFor <em>Can Sign Up For</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Can Sign Up For</em>' reference.
	 * @see #getCanSignUpFor()
	 * @generated
	 */
	void setCanSignUpFor(Course value);

	/**
	 * Returns the value of the '<em><b>Belongs To</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link coursePage.Department#getResponsibleFor <em>Responsible For</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Belongs To</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Belongs To</em>' container reference.
	 * @see #setBelongsTo(Department)
	 * @see coursePage.CoursePagePackage#getStudyProgram_BelongsTo()
	 * @see coursePage.Department#getResponsibleFor
	 * @model opposite="responsibleFor" required="true" transient="false"
	 * @generated
	 */
	Department getBelongsTo();

	/**
	 * Sets the value of the '{@link coursePage.StudyProgram#getBelongsTo <em>Belongs To</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Belongs To</em>' container reference.
	 * @see #getBelongsTo()
	 * @generated
	 */
	void setBelongsTo(Department value);
} // StudyProgram
