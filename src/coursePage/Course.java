/**
 */
package coursePage;

import java.util.Date;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.Course#getCode <em>Code</em>}</li>
 *   <li>{@link coursePage.Course#getName <em>Name</em>}</li>
 *   <li>{@link coursePage.Course#getContent <em>Content</em>}</li>
 *   <li>{@link coursePage.Course#getTimetable <em>Timetable</em>}</li>
 *   <li>{@link coursePage.Course#getRelation <em>Relation</em>}</li>
 *   <li>{@link coursePage.Course#getCredits <em>Credits</em>}</li>
 *   <li>{@link coursePage.Course#getGrading <em>Grading</em>}</li>
 *   <li>{@link coursePage.Course#getRequired <em>Required</em>}</li>
 *   <li>{@link coursePage.Course#getCourseWork <em>Course Work</em>}</li>
 *   <li>{@link coursePage.Course#getHasBeenTaught <em>Has Been Taught</em>}</li>
 *   <li>{@link coursePage.Course#getPlannedFor <em>Planned For</em>}</li>
 *   <li>{@link coursePage.Course#getSemester <em>Semester</em>}</li>
 *   <li>{@link coursePage.Course#getCourseStaff <em>Course Staff</em>}</li>
 *   <li>{@link coursePage.Course#getBelongsTo <em>Belongs To</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getCourse()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='atleastOneCourseCordinator maxHundredPoints scheduledHoursMinCourseAndLab'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 atleastOneCourseCordinator='self.courseStaff.inTheStaff.isCC != null' maxHundredPoints='self.grading.points -&gt; sum() &lt;= 100.0' scheduledHoursMinCourseAndLab='self.timetable.table.hours -&gt; sum() &gt;= self.courseWork.lectureHours-&gt;sum() + self.courseWork.labHours-&gt;sum()'"
 * @generated
 */
public interface Course extends EObject {

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see coursePage.CoursePagePackage#getCourse_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link coursePage.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see coursePage.CoursePagePackage#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link coursePage.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see coursePage.CoursePagePackage#getCourse_Content()
	 * @model
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link coursePage.Course#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Timetable</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link coursePage.TimeTable#getFor <em>For</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timetable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timetable</em>' containment reference.
	 * @see #setTimetable(TimeTable)
	 * @see coursePage.CoursePagePackage#getCourse_Timetable()
	 * @see coursePage.TimeTable#getFor
	 * @model opposite="for" containment="true" required="true"
	 * @generated
	 */
	TimeTable getTimetable();

	/**
	 * Sets the value of the '{@link coursePage.Course#getTimetable <em>Timetable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timetable</em>' containment reference.
	 * @see #getTimetable()
	 * @generated
	 */
	void setTimetable(TimeTable value);

	/**
	 * Returns the value of the '<em><b>Relation</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.RelationToCourse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getCourse_Relation()
	 * @model containment="true"
	 * @generated
	 */
	EList<RelationToCourse> getRelation();

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(double)
	 * @see coursePage.CoursePagePackage#getCourse_Credits()
	 * @model
	 * @generated
	 */
	double getCredits();

	/**
	 * Sets the value of the '{@link coursePage.Course#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(double value);

	/**
	 * Returns the value of the '<em><b>Grading</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.Evaluation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grading</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grading</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getCourse_Grading()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Evaluation> getGrading();

	/**
	 * Returns the value of the '<em><b>Required</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required</em>' reference.
	 * @see #setRequired(Course)
	 * @see coursePage.CoursePagePackage#getCourse_Required()
	 * @model
	 * @generated
	 */
	Course getRequired();

	/**
	 * Sets the value of the '{@link coursePage.Course#getRequired <em>Required</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required</em>' reference.
	 * @see #getRequired()
	 * @generated
	 */
	void setRequired(Course value);

	/**
	 * Returns the value of the '<em><b>Course Work</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Work</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Work</em>' containment reference.
	 * @see #setCourseWork(CourseWork)
	 * @see coursePage.CoursePagePackage#getCourse_CourseWork()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CourseWork getCourseWork();

	/**
	 * Sets the value of the '{@link coursePage.Course#getCourseWork <em>Course Work</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Work</em>' containment reference.
	 * @see #getCourseWork()
	 * @generated
	 */
	void setCourseWork(CourseWork value);

	/**
	 * Returns the value of the '<em><b>Has Been Taught</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.CourseInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Been Taught</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Been Taught</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getCourse_HasBeenTaught()
	 * @model containment="true"
	 * @generated
	 */
	EList<CourseInstance> getHasBeenTaught();

	/**
	 * Returns the value of the '<em><b>Planned For</b></em>' reference list.
	 * The list contents are of type {@link coursePage.StudyProgram}.
	 * It is bidirectional and its opposite is '{@link coursePage.StudyProgram#getCanSignUpFor <em>Can Sign Up For</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Planned For</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Planned For</em>' reference list.
	 * @see coursePage.CoursePagePackage#getCourse_PlannedFor()
	 * @see coursePage.StudyProgram#getCanSignUpFor
	 * @model opposite="canSignUpFor"
	 * @generated
	 */
	EList<StudyProgram> getPlannedFor();

	/**
	 * Returns the value of the '<em><b>Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semester</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester</em>' attribute.
	 * @see #setSemester(Date)
	 * @see coursePage.CoursePagePackage#getCourse_Semester()
	 * @model
	 * @generated
	 */
	Date getSemester();

	/**
	 * Sets the value of the '{@link coursePage.Course#getSemester <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester</em>' attribute.
	 * @see #getSemester()
	 * @generated
	 */
	void setSemester(Date value);

	/**
	 * Returns the value of the '<em><b>Course Staff</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.Staff}.
	 * It is bidirectional and its opposite is '{@link coursePage.Staff#getWorksFor <em>Works For</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Staff</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Staff</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getCourse_CourseStaff()
	 * @see coursePage.Staff#getWorksFor
	 * @model opposite="worksFor" containment="true" required="true"
	 * @generated
	 */
	EList<Staff> getCourseStaff();

	/**
	 * Returns the value of the '<em><b>Belongs To</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link coursePage.Department#getResponsibleForr <em>Responsible Forr</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Belongs To</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Belongs To</em>' container reference.
	 * @see #setBelongsTo(Department)
	 * @see coursePage.CoursePagePackage#getCourse_BelongsTo()
	 * @see coursePage.Department#getResponsibleForr
	 * @model opposite="responsibleForr" required="true" transient="false"
	 * @generated
	 */
	Department getBelongsTo();

	/**
	 * Sets the value of the '{@link coursePage.Course#getBelongsTo <em>Belongs To</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Belongs To</em>' container reference.
	 * @see #getBelongsTo()
	 * @generated
	 */
	void setBelongsTo(Department value);
} // Course
