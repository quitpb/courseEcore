/**
 */
package coursePage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Study Code</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see coursePage.CoursePagePackage#getStudyCode()
 * @model
 * @generated
 */
public enum StudyCode implements Enumerator {
	/**
	 * The '<em><b>MTKYB</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MTKYB_VALUE
	 * @generated
	 * @ordered
	 */
	MTKYB(4, "MTKYB", "MTKYB"), /**
	 * The '<em><b>MTINFOSYS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MTINFOSYS_VALUE
	 * @generated
	 * @ordered
	 */
	MTINFOSYS(3, "MTINFOSYS", "MTINFOSYS"), /**
	 * The '<em><b>MTKOM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MTKOM_VALUE
	 * @generated
	 * @ordered
	 */
	MTKOM(2, "MTKOM", "MTKOM"), /**
	 * The '<em><b>MTDT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MTDT_VALUE
	 * @generated
	 * @ordered
	 */
	MTDT(1, "MTDT", "MTDT")
	;

	/**
	 * The '<em><b>MTKYB</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MTKYB</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MTKYB
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MTKYB_VALUE = 4;

/**
	 * The '<em><b>MTINFOSYS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MTINFOSYS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MTINFOSYS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MTINFOSYS_VALUE = 3;

/**
	 * The '<em><b>MTKOM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MTKOM</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MTKOM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MTKOM_VALUE = 2;

/**
	 * The '<em><b>MTDT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MTDT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MTDT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MTDT_VALUE = 1;

	/**
	 * An array of all the '<em><b>Study Code</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final StudyCode[] VALUES_ARRAY =
		new StudyCode[] {
			MTKYB,
			MTINFOSYS,
			MTKOM,
			MTDT,
		};

	/**
	 * A public read-only list of all the '<em><b>Study Code</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<StudyCode> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Study Code</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static StudyCode get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			StudyCode result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Study Code</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static StudyCode getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			StudyCode result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Study Code</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static StudyCode get(int value) {
		switch (value) {
			case MTKYB_VALUE: return MTKYB;
			case MTINFOSYS_VALUE: return MTINFOSYS;
			case MTKOM_VALUE: return MTKOM;
			case MTDT_VALUE: return MTDT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private StudyCode(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //StudyCode
