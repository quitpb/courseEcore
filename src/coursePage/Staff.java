/**
 */
package coursePage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Staff</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.Staff#getInTheStaff <em>In The Staff</em>}</li>
 *   <li>{@link coursePage.Staff#getWorksFor <em>Works For</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getStaff()
 * @model
 * @generated
 */
public interface Staff extends EObject {
	/**
	 * Returns the value of the '<em><b>In The Staff</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.Employee}.
	 * It is bidirectional and its opposite is '{@link coursePage.Employee#getInThis <em>In This</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In The Staff</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In The Staff</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getStaff_InTheStaff()
	 * @see coursePage.Employee#getInThis
	 * @model opposite="inThis" containment="true"
	 * @generated
	 */
	EList<Employee> getInTheStaff();

	/**
	 * Returns the value of the '<em><b>Works For</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link coursePage.Course#getCourseStaff <em>Course Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Works For</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Works For</em>' container reference.
	 * @see #setWorksFor(Course)
	 * @see coursePage.CoursePagePackage#getStaff_WorksFor()
	 * @see coursePage.Course#getCourseStaff
	 * @model opposite="courseStaff" transient="false"
	 * @generated
	 */
	Course getWorksFor();

	/**
	 * Sets the value of the '{@link coursePage.Staff#getWorksFor <em>Works For</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Works For</em>' container reference.
	 * @see #getWorksFor()
	 * @generated
	 */
	void setWorksFor(Course value);

} // Staff
