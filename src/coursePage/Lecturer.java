/**
 */
package coursePage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lecturer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.Lecturer#getRole <em>Role</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getLecturer()
 * @model
 * @generated
 */
public interface Lecturer extends EObject {
	/**
	 * Returns the value of the '<em><b>Role</b></em>' attribute.
	 * The literals are from the enumeration {@link coursePage.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' attribute.
	 * @see coursePage.Role
	 * @see #setRole(Role)
	 * @see coursePage.CoursePagePackage#getLecturer_Role()
	 * @model
	 * @generated
	 */
	Role getRole();

	/**
	 * Sets the value of the '{@link coursePage.Lecturer#getRole <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' attribute.
	 * @see coursePage.Role
	 * @see #getRole()
	 * @generated
	 */
	void setRole(Role value);

} // Lecturer
