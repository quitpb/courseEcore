/**
 */
package coursePage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relation To Other Courses</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.RelationToOtherCourses#getRelation <em>Relation</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getRelationToOtherCourses()
 * @model
 * @generated
 */
public interface RelationToOtherCourses extends EObject {
	/**
	 * Returns the value of the '<em><b>Relation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation</em>' reference.
	 * @see #setRelation(credits)
	 * @see coursePage.CoursePagePackage#getRelationToOtherCourses_Relation()
	 * @model
	 * @generated
	 */
	credits getRelation();

	/**
	 * Sets the value of the '{@link coursePage.RelationToOtherCourses#getRelation <em>Relation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relation</em>' reference.
	 * @see #getRelation()
	 * @generated
	 */
	void setRelation(credits value);

} // RelationToOtherCourses
