/**
 */
package coursePage;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Information</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.Information#getDay <em>Day</em>}</li>
 *   <li>{@link coursePage.Information#getRoom <em>Room</em>}</li>
 *   <li>{@link coursePage.Information#getPlannedFor <em>Planned For</em>}</li>
 *   <li>{@link coursePage.Information#getLectureType <em>Lecture Type</em>}</li>
 *   <li>{@link coursePage.Information#getStarteTime <em>Starte Time</em>}</li>
 *   <li>{@link coursePage.Information#getEndTime <em>End Time</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getInformation()
 * @model
 * @generated
 */
public interface Information extends EObject {
	/**
	 * Returns the value of the '<em><b>Day</b></em>' attribute.
	 * The literals are from the enumeration {@link coursePage.Day}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Day</em>' attribute.
	 * @see coursePage.Day
	 * @see #setDay(Day)
	 * @see coursePage.CoursePagePackage#getInformation_Day()
	 * @model
	 * @generated
	 */
	Day getDay();

	/**
	 * Sets the value of the '{@link coursePage.Information#getDay <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Day</em>' attribute.
	 * @see coursePage.Day
	 * @see #getDay()
	 * @generated
	 */
	void setDay(Day value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' attribute.
	 * The literals are from the enumeration {@link coursePage.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' attribute.
	 * @see coursePage.Room
	 * @see #setRoom(Room)
	 * @see coursePage.CoursePagePackage#getInformation_Room()
	 * @model
	 * @generated
	 */
	Room getRoom();

	/**
	 * Sets the value of the '{@link coursePage.Information#getRoom <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' attribute.
	 * @see coursePage.Room
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(Room value);

	/**
	 * Returns the value of the '<em><b>Planned For</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.StudyProgram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Planned For</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Planned For</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getInformation_PlannedFor()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<StudyProgram> getPlannedFor();

	/**
	 * Returns the value of the '<em><b>Lecture Type</b></em>' attribute.
	 * The literals are from the enumeration {@link coursePage.LectureType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecture Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecture Type</em>' attribute.
	 * @see coursePage.LectureType
	 * @see #setLectureType(LectureType)
	 * @see coursePage.CoursePagePackage#getInformation_LectureType()
	 * @model
	 * @generated
	 */
	LectureType getLectureType();

	/**
	 * Sets the value of the '{@link coursePage.Information#getLectureType <em>Lecture Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lecture Type</em>' attribute.
	 * @see coursePage.LectureType
	 * @see #getLectureType()
	 * @generated
	 */
	void setLectureType(LectureType value);

	/**
	 * Returns the value of the '<em><b>Starte Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Starte Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Starte Time</em>' attribute.
	 * @see #setStarteTime(Date)
	 * @see coursePage.CoursePagePackage#getInformation_StarteTime()
	 * @model
	 * @generated
	 */
	Date getStarteTime();

	/**
	 * Sets the value of the '{@link coursePage.Information#getStarteTime <em>Starte Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Starte Time</em>' attribute.
	 * @see #getStarteTime()
	 * @generated
	 */
	void setStarteTime(Date value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(Date)
	 * @see coursePage.CoursePagePackage#getInformation_EndTime()
	 * @model
	 * @generated
	 */
	Date getEndTime();

	/**
	 * Sets the value of the '{@link coursePage.Information#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(Date value);

} // Information
