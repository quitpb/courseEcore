/**
 */
package coursePage;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Organization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.Organization#getName <em>Name</em>}</li>
 *   <li>{@link coursePage.Organization#getHas <em>Has</em>}</li>
 *   <li>{@link coursePage.Organization#getResponsibleFor <em>Responsible For</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getOrganization()
 * @model
 * @generated
 */
public interface Organization extends EObject {

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see coursePage.CoursePagePackage#getOrganization_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link coursePage.Organization#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Has</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.Employee}.
	 * It is bidirectional and its opposite is '{@link coursePage.Employee#getWorksFor <em>Works For</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getOrganization_Has()
	 * @see coursePage.Employee#getWorksFor
	 * @model opposite="WorksFor" containment="true" required="true"
	 * @generated
	 */
	EList<Employee> getHas();

	/**
	 * Returns the value of the '<em><b>Responsible For</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible For</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible For</em>' containment reference.
	 * @see #setResponsibleFor(Course)
	 * @see coursePage.CoursePagePackage#getOrganization_ResponsibleFor()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Course getResponsibleFor();

	/**
	 * Sets the value of the '{@link coursePage.Organization#getResponsibleFor <em>Responsible For</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Responsible For</em>' containment reference.
	 * @see #getResponsibleFor()
	 * @generated
	 */
	void setResponsibleFor(Course value);
} // Organization
