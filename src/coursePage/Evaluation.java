/**
 */
package coursePage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.Evaluation#getEvaluation <em>Evaluation</em>}</li>
 *   <li>{@link coursePage.Evaluation#getPoints <em>Points</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getEvaluation()
 * @model
 * @generated
 */
public interface Evaluation extends EObject {
	/**
	 * Returns the value of the '<em><b>Evaluation</b></em>' attribute.
	 * The literals are from the enumeration {@link coursePage.EvaluationForm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluation</em>' attribute.
	 * @see coursePage.EvaluationForm
	 * @see #setEvaluation(EvaluationForm)
	 * @see coursePage.CoursePagePackage#getEvaluation_Evaluation()
	 * @model
	 * @generated
	 */
	EvaluationForm getEvaluation();

	/**
	 * Sets the value of the '{@link coursePage.Evaluation#getEvaluation <em>Evaluation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluation</em>' attribute.
	 * @see coursePage.EvaluationForm
	 * @see #getEvaluation()
	 * @generated
	 */
	void setEvaluation(EvaluationForm value);

	/**
	 * Returns the value of the '<em><b>Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Points</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Points</em>' attribute.
	 * @see #setPoints(double)
	 * @see coursePage.CoursePagePackage#getEvaluation_Points()
	 * @model
	 * @generated
	 */
	double getPoints();

	/**
	 * Sets the value of the '{@link coursePage.Evaluation#getPoints <em>Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Points</em>' attribute.
	 * @see #getPoints()
	 * @generated
	 */
	void setPoints(double value);

} // Evaluation
