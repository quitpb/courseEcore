/**
 */
package coursePage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Slot</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.Slot#getDay <em>Day</em>}</li>
 *   <li>{@link coursePage.Slot#getRoom <em>Room</em>}</li>
 *   <li>{@link coursePage.Slot#getLectureType <em>Lecture Type</em>}</li>
 *   <li>{@link coursePage.Slot#getStarteTime <em>Starte Time</em>}</li>
 *   <li>{@link coursePage.Slot#getEndTime <em>End Time</em>}</li>
 *   <li>{@link coursePage.Slot#getScheduleFor <em>Schedule For</em>}</li>
 *   <li>{@link coursePage.Slot#getHours <em>Hours</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getSlot()
 * @model
 * @generated
 */
public interface Slot extends EObject {
	/**
	 * Returns the value of the '<em><b>Day</b></em>' attribute.
	 * The literals are from the enumeration {@link coursePage.Day}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Day</em>' attribute.
	 * @see coursePage.Day
	 * @see #setDay(Day)
	 * @see coursePage.CoursePagePackage#getSlot_Day()
	 * @model
	 * @generated
	 */
	Day getDay();

	/**
	 * Sets the value of the '{@link coursePage.Slot#getDay <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Day</em>' attribute.
	 * @see coursePage.Day
	 * @see #getDay()
	 * @generated
	 */
	void setDay(Day value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' attribute.
	 * The literals are from the enumeration {@link coursePage.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' attribute.
	 * @see coursePage.Room
	 * @see #setRoom(Room)
	 * @see coursePage.CoursePagePackage#getSlot_Room()
	 * @model
	 * @generated
	 */
	Room getRoom();

	/**
	 * Sets the value of the '{@link coursePage.Slot#getRoom <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' attribute.
	 * @see coursePage.Room
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(Room value);

	/**
	 * Returns the value of the '<em><b>Lecture Type</b></em>' attribute.
	 * The literals are from the enumeration {@link coursePage.LectureType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecture Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecture Type</em>' attribute.
	 * @see coursePage.LectureType
	 * @see #setLectureType(LectureType)
	 * @see coursePage.CoursePagePackage#getSlot_LectureType()
	 * @model
	 * @generated
	 */
	LectureType getLectureType();

	/**
	 * Sets the value of the '{@link coursePage.Slot#getLectureType <em>Lecture Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lecture Type</em>' attribute.
	 * @see coursePage.LectureType
	 * @see #getLectureType()
	 * @generated
	 */
	void setLectureType(LectureType value);

	/**
	 * Returns the value of the '<em><b>Starte Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Starte Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Starte Time</em>' attribute.
	 * @see #setStarteTime(String)
	 * @see coursePage.CoursePagePackage#getSlot_StarteTime()
	 * @model
	 * @generated
	 */
	String getStarteTime();

	/**
	 * Sets the value of the '{@link coursePage.Slot#getStarteTime <em>Starte Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Starte Time</em>' attribute.
	 * @see #getStarteTime()
	 * @generated
	 */
	void setStarteTime(String value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(String)
	 * @see coursePage.CoursePagePackage#getSlot_EndTime()
	 * @model
	 * @generated
	 */
	String getEndTime();

	/**
	 * Sets the value of the '{@link coursePage.Slot#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(String value);

	/**
	 * Returns the value of the '<em><b>Schedule For</b></em>' reference list.
	 * The list contents are of type {@link coursePage.StudyProgram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schedule For</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedule For</em>' reference list.
	 * @see coursePage.CoursePagePackage#getSlot_ScheduleFor()
	 * @model required="true"
	 * @generated
	 */
	EList<StudyProgram> getScheduleFor();

	/**
	 * Returns the value of the '<em><b>Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hours</em>' attribute.
	 * @see #setHours(double)
	 * @see coursePage.CoursePagePackage#getSlot_Hours()
	 * @model
	 * @generated
	 */
	double getHours();

	/**
	 * Sets the value of the '{@link coursePage.Slot#getHours <em>Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hours</em>' attribute.
	 * @see #getHours()
	 * @generated
	 */
	void setHours(double value);

} // Slot
