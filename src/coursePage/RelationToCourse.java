/**
 */
package coursePage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relation To Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.RelationToCourse#getCourse <em>Course</em>}</li>
 *   <li>{@link coursePage.RelationToCourse#getCreditsReduction <em>Credits Reduction</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getRelationToCourse()
 * @model
 * @generated
 */
public interface RelationToCourse extends EObject {
	/**
	 * Returns the value of the '<em><b>Course</b></em>' attribute.
	 * The literals are from the enumeration {@link coursePage.RelatedCourse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' attribute.
	 * @see coursePage.RelatedCourse
	 * @see #setCourse(RelatedCourse)
	 * @see coursePage.CoursePagePackage#getRelationToCourse_Course()
	 * @model
	 * @generated
	 */
	RelatedCourse getCourse();

	/**
	 * Sets the value of the '{@link coursePage.RelationToCourse#getCourse <em>Course</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' attribute.
	 * @see coursePage.RelatedCourse
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(RelatedCourse value);

	/**
	 * Returns the value of the '<em><b>Credits Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits Reduction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits Reduction</em>' attribute.
	 * @see #setCreditsReduction(double)
	 * @see coursePage.CoursePagePackage#getRelationToCourse_CreditsReduction()
	 * @model
	 * @generated
	 */
	double getCreditsReduction();

	/**
	 * Sets the value of the '{@link coursePage.RelationToCourse#getCreditsReduction <em>Credits Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits Reduction</em>' attribute.
	 * @see #getCreditsReduction()
	 * @generated
	 */
	void setCreditsReduction(double value);

} // RelationToCourse
