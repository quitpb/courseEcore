/**
 */
package coursePage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>credits</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.credits#getPoints <em>Points</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getcredits()
 * @model
 * @generated
 */
public interface credits extends EObject {
	/**
	 * Returns the value of the '<em><b>Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Points</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Points</em>' attribute.
	 * @see #setPoints(double)
	 * @see coursePage.CoursePagePackage#getcredits_Points()
	 * @model
	 * @generated
	 */
	double getPoints();

	/**
	 * Sets the value of the '{@link coursePage.credits#getPoints <em>Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Points</em>' attribute.
	 * @see #getPoints()
	 * @generated
	 */
	void setPoints(double value);

} // credits
