/**
 */
package coursePage;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.TimeTable#getTable <em>Table</em>}</li>
 *   <li>{@link coursePage.TimeTable#getFor <em>For</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getTimeTable()
 * @model
 * @generated
 */
public interface TimeTable extends EObject {

	/**
	 * Returns the value of the '<em><b>Table</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.Slot}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Table</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Table</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getTimeTable_Table()
	 * @model containment="true"
	 * @generated
	 */
	EList<Slot> getTable();

	/**
	 * Returns the value of the '<em><b>For</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link coursePage.Course#getTimetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For</em>' container reference.
	 * @see #setFor(Course)
	 * @see coursePage.CoursePagePackage#getTimeTable_For()
	 * @see coursePage.Course#getTimetable
	 * @model opposite="timetable" required="true" transient="false"
	 * @generated
	 */
	Course getFor();

	/**
	 * Sets the value of the '{@link coursePage.TimeTable#getFor <em>For</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>For</em>' container reference.
	 * @see #getFor()
	 * @generated
	 */
	void setFor(Course value);
} // TimeTable
