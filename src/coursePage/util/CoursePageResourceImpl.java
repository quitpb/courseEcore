/**
 */
package coursePage.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see coursePage.util.CoursePageResourceFactoryImpl
 * @generated
 */
public class CoursePageResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public CoursePageResourceImpl(URI uri) {
		super(uri);
	}

	CoursePageResourceImpl() {
	  throw new UnsupportedOperationException("TODO: auto-generated method stub");
	}

} //CoursePageResourceImpl
