/**
 */
package coursePage.impl;

import coursePage.CoursePagePackage;
import coursePage.RelatedCourse;
import coursePage.RelationToCourse;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relation To Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coursePage.impl.RelationToCourseImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link coursePage.impl.RelationToCourseImpl#getCreditsReduction <em>Credits Reduction</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RelationToCourseImpl extends MinimalEObjectImpl.Container implements RelationToCourse {
	/**
	 * The default value of the '{@link #getCourse() <em>Course</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourse()
	 * @generated
	 * @ordered
	 */
	protected static final RelatedCourse COURSE_EDEFAULT = RelatedCourse.SIF8060;

	/**
	 * The cached value of the '{@link #getCourse() <em>Course</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourse()
	 * @generated
	 * @ordered
	 */
	protected RelatedCourse course = COURSE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCreditsReduction() <em>Credits Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditsReduction()
	 * @generated
	 * @ordered
	 */
	protected static final double CREDITS_REDUCTION_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCreditsReduction() <em>Credits Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditsReduction()
	 * @generated
	 * @ordered
	 */
	protected double creditsReduction = CREDITS_REDUCTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RelationToCourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePagePackage.Literals.RELATION_TO_COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelatedCourse getCourse() {
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(RelatedCourse newCourse) {
		RelatedCourse oldCourse = course;
		course = newCourse == null ? COURSE_EDEFAULT : newCourse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.RELATION_TO_COURSE__COURSE, oldCourse, course));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCreditsReduction() {
		return creditsReduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreditsReduction(double newCreditsReduction) {
		double oldCreditsReduction = creditsReduction;
		creditsReduction = newCreditsReduction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.RELATION_TO_COURSE__CREDITS_REDUCTION, oldCreditsReduction, creditsReduction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePagePackage.RELATION_TO_COURSE__COURSE:
				return getCourse();
			case CoursePagePackage.RELATION_TO_COURSE__CREDITS_REDUCTION:
				return getCreditsReduction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePagePackage.RELATION_TO_COURSE__COURSE:
				setCourse((RelatedCourse)newValue);
				return;
			case CoursePagePackage.RELATION_TO_COURSE__CREDITS_REDUCTION:
				setCreditsReduction((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePagePackage.RELATION_TO_COURSE__COURSE:
				setCourse(COURSE_EDEFAULT);
				return;
			case CoursePagePackage.RELATION_TO_COURSE__CREDITS_REDUCTION:
				setCreditsReduction(CREDITS_REDUCTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePagePackage.RELATION_TO_COURSE__COURSE:
				return course != COURSE_EDEFAULT;
			case CoursePagePackage.RELATION_TO_COURSE__CREDITS_REDUCTION:
				return creditsReduction != CREDITS_REDUCTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (course: ");
		result.append(course);
		result.append(", creditsReduction: ");
		result.append(creditsReduction);
		result.append(')');
		return result.toString();
	}

} //RelationToCourseImpl
