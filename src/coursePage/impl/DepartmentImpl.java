/**
 */
package coursePage.impl;

import coursePage.Course;
import coursePage.CoursePagePackage;
import coursePage.Department;
import coursePage.Employee;
import coursePage.StudyProgram;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coursePage.impl.DepartmentImpl#getName <em>Name</em>}</li>
 *   <li>{@link coursePage.impl.DepartmentImpl#getHas <em>Has</em>}</li>
 *   <li>{@link coursePage.impl.DepartmentImpl#getResponsibleForr <em>Responsible Forr</em>}</li>
 *   <li>{@link coursePage.impl.DepartmentImpl#getResponsibleFor <em>Responsible For</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DepartmentImpl extends MinimalEObjectImpl.Container implements Department {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHas() <em>Has</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHas()
	 * @generated
	 * @ordered
	 */
	protected EList<Employee> has;

	/**
	 * The cached value of the '{@link #getResponsibleForr() <em>Responsible Forr</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleForr()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> responsibleForr;

	/**
	 * The cached value of the '{@link #getResponsibleFor() <em>Responsible For</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleFor()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> responsibleFor;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DepartmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePagePackage.Literals.DEPARTMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.DEPARTMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Employee> getHas() {
		if (has == null) {
			has = new EObjectContainmentWithInverseEList<Employee>(Employee.class, this, CoursePagePackage.DEPARTMENT__HAS, CoursePagePackage.EMPLOYEE__WORKS_FOR);
		}
		return has;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getResponsibleForr() {
		if (responsibleForr == null) {
			responsibleForr = new EObjectContainmentWithInverseEList<Course>(Course.class, this, CoursePagePackage.DEPARTMENT__RESPONSIBLE_FORR, CoursePagePackage.COURSE__BELONGS_TO);
		}
		return responsibleForr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getResponsibleFor() {
		if (responsibleFor == null) {
			responsibleFor = new EObjectContainmentWithInverseEList<StudyProgram>(StudyProgram.class, this, CoursePagePackage.DEPARTMENT__RESPONSIBLE_FOR, CoursePagePackage.STUDY_PROGRAM__BELONGS_TO);
		}
		return responsibleFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.DEPARTMENT__HAS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getHas()).basicAdd(otherEnd, msgs);
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FORR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getResponsibleForr()).basicAdd(otherEnd, msgs);
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FOR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getResponsibleFor()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.DEPARTMENT__HAS:
				return ((InternalEList<?>)getHas()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FORR:
				return ((InternalEList<?>)getResponsibleForr()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FOR:
				return ((InternalEList<?>)getResponsibleFor()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePagePackage.DEPARTMENT__NAME:
				return getName();
			case CoursePagePackage.DEPARTMENT__HAS:
				return getHas();
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FORR:
				return getResponsibleForr();
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FOR:
				return getResponsibleFor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePagePackage.DEPARTMENT__NAME:
				setName((String)newValue);
				return;
			case CoursePagePackage.DEPARTMENT__HAS:
				getHas().clear();
				getHas().addAll((Collection<? extends Employee>)newValue);
				return;
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FORR:
				getResponsibleForr().clear();
				getResponsibleForr().addAll((Collection<? extends Course>)newValue);
				return;
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FOR:
				getResponsibleFor().clear();
				getResponsibleFor().addAll((Collection<? extends StudyProgram>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePagePackage.DEPARTMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CoursePagePackage.DEPARTMENT__HAS:
				getHas().clear();
				return;
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FORR:
				getResponsibleForr().clear();
				return;
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FOR:
				getResponsibleFor().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePagePackage.DEPARTMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CoursePagePackage.DEPARTMENT__HAS:
				return has != null && !has.isEmpty();
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FORR:
				return responsibleForr != null && !responsibleForr.isEmpty();
			case CoursePagePackage.DEPARTMENT__RESPONSIBLE_FOR:
				return responsibleFor != null && !responsibleFor.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DepartmentImpl
