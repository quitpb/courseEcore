/**
 */
package coursePage.impl;

import coursePage.Course;
import coursePage.CourseInstance;
import coursePage.CoursePagePackage;

import coursePage.CourseWork;
import coursePage.Department;
import coursePage.Evaluation;
import coursePage.RelationToCourse;
import coursePage.Staff;
import coursePage.StudyProgram;
import coursePage.TimeTable;
import java.util.Collection;
import java.util.Date;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coursePage.impl.CourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getContent <em>Content</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getTimetable <em>Timetable</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getRelation <em>Relation</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getCredits <em>Credits</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getGrading <em>Grading</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getRequired <em>Required</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getCourseWork <em>Course Work</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getHasBeenTaught <em>Has Been Taught</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getPlannedFor <em>Planned For</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getSemester <em>Semester</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getCourseStaff <em>Course Staff</em>}</li>
 *   <li>{@link coursePage.impl.CourseImpl#getBelongsTo <em>Belongs To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;
	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;
	/**
	 * The cached value of the '{@link #getTimetable() <em>Timetable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimetable()
	 * @generated
	 * @ordered
	 */
	protected TimeTable timetable;
	/**
	 * The cached value of the '{@link #getRelation() <em>Relation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelation()
	 * @generated
	 * @ordered
	 */
	protected EList<RelationToCourse> relation;

	/**
	 * The default value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected static final double CREDITS_EDEFAULT = 0.0;
	/**
	 * The cached value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected double credits = CREDITS_EDEFAULT;
	/**
	 * The cached value of the '{@link #getGrading() <em>Grading</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGrading()
	 * @generated
	 * @ordered
	 */
	protected EList<Evaluation> grading;
	/**
	 * The cached value of the '{@link #getRequired() <em>Required</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequired()
	 * @generated
	 * @ordered
	 */
	protected Course required;
	/**
	 * The cached value of the '{@link #getCourseWork() <em>Course Work</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseWork()
	 * @generated
	 * @ordered
	 */
	protected CourseWork courseWork;
	/**
	 * The cached value of the '{@link #getHasBeenTaught() <em>Has Been Taught</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasBeenTaught()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> hasBeenTaught;

	/**
	 * The cached value of the '{@link #getPlannedFor() <em>Planned For</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlannedFor()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> plannedFor;
	/**
	 * The default value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected static final Date SEMESTER_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected Date semester = SEMESTER_EDEFAULT;
	/**
	 * The cached value of the '{@link #getCourseStaff() <em>Course Staff</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseStaff()
	 * @generated
	 * @ordered
	 */
	protected EList<Staff> courseStaff;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePagePackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeTable getTimetable() {
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimetable(TimeTable newTimetable, NotificationChain msgs) {
		TimeTable oldTimetable = timetable;
		timetable = newTimetable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__TIMETABLE, oldTimetable, newTimetable);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimetable(TimeTable newTimetable) {
		if (newTimetable != timetable) {
			NotificationChain msgs = null;
			if (timetable != null)
				msgs = ((InternalEObject)timetable).eInverseRemove(this, CoursePagePackage.TIME_TABLE__FOR, TimeTable.class, msgs);
			if (newTimetable != null)
				msgs = ((InternalEObject)newTimetable).eInverseAdd(this, CoursePagePackage.TIME_TABLE__FOR, TimeTable.class, msgs);
			msgs = basicSetTimetable(newTimetable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__TIMETABLE, newTimetable, newTimetable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RelationToCourse> getRelation() {
		if (relation == null) {
			relation = new EObjectContainmentEList<RelationToCourse>(RelationToCourse.class, this, CoursePagePackage.COURSE__RELATION);
		}
		return relation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCredits() {
		return credits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCredits(double newCredits) {
		double oldCredits = credits;
		credits = newCredits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__CREDITS, oldCredits, credits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Evaluation> getGrading() {
		if (grading == null) {
			grading = new EObjectContainmentEList<Evaluation>(Evaluation.class, this, CoursePagePackage.COURSE__GRADING);
		}
		return grading;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getRequired() {
		if (required != null && required.eIsProxy()) {
			InternalEObject oldRequired = (InternalEObject)required;
			required = (Course)eResolveProxy(oldRequired);
			if (required != oldRequired) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePagePackage.COURSE__REQUIRED, oldRequired, required));
			}
		}
		return required;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course basicGetRequired() {
		return required;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequired(Course newRequired) {
		Course oldRequired = required;
		required = newRequired;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__REQUIRED, oldRequired, required));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWork getCourseWork() {
		return courseWork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseWork(CourseWork newCourseWork, NotificationChain msgs) {
		CourseWork oldCourseWork = courseWork;
		courseWork = newCourseWork;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__COURSE_WORK, oldCourseWork, newCourseWork);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseWork(CourseWork newCourseWork) {
		if (newCourseWork != courseWork) {
			NotificationChain msgs = null;
			if (courseWork != null)
				msgs = ((InternalEObject)courseWork).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoursePagePackage.COURSE__COURSE_WORK, null, msgs);
			if (newCourseWork != null)
				msgs = ((InternalEObject)newCourseWork).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CoursePagePackage.COURSE__COURSE_WORK, null, msgs);
			msgs = basicSetCourseWork(newCourseWork, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__COURSE_WORK, newCourseWork, newCourseWork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getHasBeenTaught() {
		if (hasBeenTaught == null) {
			hasBeenTaught = new EObjectContainmentEList<CourseInstance>(CourseInstance.class, this, CoursePagePackage.COURSE__HAS_BEEN_TAUGHT);
		}
		return hasBeenTaught;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getPlannedFor() {
		if (plannedFor == null) {
			plannedFor = new EObjectWithInverseResolvingEList<StudyProgram>(StudyProgram.class, this, CoursePagePackage.COURSE__PLANNED_FOR, CoursePagePackage.STUDY_PROGRAM__CAN_SIGN_UP_FOR);
		}
		return plannedFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getSemester() {
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemester(Date newSemester) {
		Date oldSemester = semester;
		semester = newSemester;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__SEMESTER, oldSemester, semester));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Staff> getCourseStaff() {
		if (courseStaff == null) {
			courseStaff = new EObjectContainmentWithInverseEList<Staff>(Staff.class, this, CoursePagePackage.COURSE__COURSE_STAFF, CoursePagePackage.STAFF__WORKS_FOR);
		}
		return courseStaff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getBelongsTo() {
		if (eContainerFeatureID() != CoursePagePackage.COURSE__BELONGS_TO) return null;
		return (Department)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBelongsTo(Department newBelongsTo, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newBelongsTo, CoursePagePackage.COURSE__BELONGS_TO, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBelongsTo(Department newBelongsTo) {
		if (newBelongsTo != eInternalContainer() || (eContainerFeatureID() != CoursePagePackage.COURSE__BELONGS_TO && newBelongsTo != null)) {
			if (EcoreUtil.isAncestor(this, newBelongsTo))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newBelongsTo != null)
				msgs = ((InternalEObject)newBelongsTo).eInverseAdd(this, CoursePagePackage.DEPARTMENT__RESPONSIBLE_FORR, Department.class, msgs);
			msgs = basicSetBelongsTo(newBelongsTo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.COURSE__BELONGS_TO, newBelongsTo, newBelongsTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.COURSE__TIMETABLE:
				if (timetable != null)
					msgs = ((InternalEObject)timetable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoursePagePackage.COURSE__TIMETABLE, null, msgs);
				return basicSetTimetable((TimeTable)otherEnd, msgs);
			case CoursePagePackage.COURSE__PLANNED_FOR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPlannedFor()).basicAdd(otherEnd, msgs);
			case CoursePagePackage.COURSE__COURSE_STAFF:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCourseStaff()).basicAdd(otherEnd, msgs);
			case CoursePagePackage.COURSE__BELONGS_TO:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetBelongsTo((Department)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.COURSE__TIMETABLE:
				return basicSetTimetable(null, msgs);
			case CoursePagePackage.COURSE__RELATION:
				return ((InternalEList<?>)getRelation()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.COURSE__GRADING:
				return ((InternalEList<?>)getGrading()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.COURSE__COURSE_WORK:
				return basicSetCourseWork(null, msgs);
			case CoursePagePackage.COURSE__HAS_BEEN_TAUGHT:
				return ((InternalEList<?>)getHasBeenTaught()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.COURSE__PLANNED_FOR:
				return ((InternalEList<?>)getPlannedFor()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.COURSE__COURSE_STAFF:
				return ((InternalEList<?>)getCourseStaff()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.COURSE__BELONGS_TO:
				return basicSetBelongsTo(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CoursePagePackage.COURSE__BELONGS_TO:
				return eInternalContainer().eInverseRemove(this, CoursePagePackage.DEPARTMENT__RESPONSIBLE_FORR, Department.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePagePackage.COURSE__CODE:
				return getCode();
			case CoursePagePackage.COURSE__NAME:
				return getName();
			case CoursePagePackage.COURSE__CONTENT:
				return getContent();
			case CoursePagePackage.COURSE__TIMETABLE:
				return getTimetable();
			case CoursePagePackage.COURSE__RELATION:
				return getRelation();
			case CoursePagePackage.COURSE__CREDITS:
				return getCredits();
			case CoursePagePackage.COURSE__GRADING:
				return getGrading();
			case CoursePagePackage.COURSE__REQUIRED:
				if (resolve) return getRequired();
				return basicGetRequired();
			case CoursePagePackage.COURSE__COURSE_WORK:
				return getCourseWork();
			case CoursePagePackage.COURSE__HAS_BEEN_TAUGHT:
				return getHasBeenTaught();
			case CoursePagePackage.COURSE__PLANNED_FOR:
				return getPlannedFor();
			case CoursePagePackage.COURSE__SEMESTER:
				return getSemester();
			case CoursePagePackage.COURSE__COURSE_STAFF:
				return getCourseStaff();
			case CoursePagePackage.COURSE__BELONGS_TO:
				return getBelongsTo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePagePackage.COURSE__CODE:
				setCode((String)newValue);
				return;
			case CoursePagePackage.COURSE__NAME:
				setName((String)newValue);
				return;
			case CoursePagePackage.COURSE__CONTENT:
				setContent((String)newValue);
				return;
			case CoursePagePackage.COURSE__TIMETABLE:
				setTimetable((TimeTable)newValue);
				return;
			case CoursePagePackage.COURSE__RELATION:
				getRelation().clear();
				getRelation().addAll((Collection<? extends RelationToCourse>)newValue);
				return;
			case CoursePagePackage.COURSE__CREDITS:
				setCredits((Double)newValue);
				return;
			case CoursePagePackage.COURSE__GRADING:
				getGrading().clear();
				getGrading().addAll((Collection<? extends Evaluation>)newValue);
				return;
			case CoursePagePackage.COURSE__REQUIRED:
				setRequired((Course)newValue);
				return;
			case CoursePagePackage.COURSE__COURSE_WORK:
				setCourseWork((CourseWork)newValue);
				return;
			case CoursePagePackage.COURSE__HAS_BEEN_TAUGHT:
				getHasBeenTaught().clear();
				getHasBeenTaught().addAll((Collection<? extends CourseInstance>)newValue);
				return;
			case CoursePagePackage.COURSE__PLANNED_FOR:
				getPlannedFor().clear();
				getPlannedFor().addAll((Collection<? extends StudyProgram>)newValue);
				return;
			case CoursePagePackage.COURSE__SEMESTER:
				setSemester((Date)newValue);
				return;
			case CoursePagePackage.COURSE__COURSE_STAFF:
				getCourseStaff().clear();
				getCourseStaff().addAll((Collection<? extends Staff>)newValue);
				return;
			case CoursePagePackage.COURSE__BELONGS_TO:
				setBelongsTo((Department)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePagePackage.COURSE__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case CoursePagePackage.COURSE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CoursePagePackage.COURSE__CONTENT:
				setContent(CONTENT_EDEFAULT);
				return;
			case CoursePagePackage.COURSE__TIMETABLE:
				setTimetable((TimeTable)null);
				return;
			case CoursePagePackage.COURSE__RELATION:
				getRelation().clear();
				return;
			case CoursePagePackage.COURSE__CREDITS:
				setCredits(CREDITS_EDEFAULT);
				return;
			case CoursePagePackage.COURSE__GRADING:
				getGrading().clear();
				return;
			case CoursePagePackage.COURSE__REQUIRED:
				setRequired((Course)null);
				return;
			case CoursePagePackage.COURSE__COURSE_WORK:
				setCourseWork((CourseWork)null);
				return;
			case CoursePagePackage.COURSE__HAS_BEEN_TAUGHT:
				getHasBeenTaught().clear();
				return;
			case CoursePagePackage.COURSE__PLANNED_FOR:
				getPlannedFor().clear();
				return;
			case CoursePagePackage.COURSE__SEMESTER:
				setSemester(SEMESTER_EDEFAULT);
				return;
			case CoursePagePackage.COURSE__COURSE_STAFF:
				getCourseStaff().clear();
				return;
			case CoursePagePackage.COURSE__BELONGS_TO:
				setBelongsTo((Department)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePagePackage.COURSE__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case CoursePagePackage.COURSE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CoursePagePackage.COURSE__CONTENT:
				return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
			case CoursePagePackage.COURSE__TIMETABLE:
				return timetable != null;
			case CoursePagePackage.COURSE__RELATION:
				return relation != null && !relation.isEmpty();
			case CoursePagePackage.COURSE__CREDITS:
				return credits != CREDITS_EDEFAULT;
			case CoursePagePackage.COURSE__GRADING:
				return grading != null && !grading.isEmpty();
			case CoursePagePackage.COURSE__REQUIRED:
				return required != null;
			case CoursePagePackage.COURSE__COURSE_WORK:
				return courseWork != null;
			case CoursePagePackage.COURSE__HAS_BEEN_TAUGHT:
				return hasBeenTaught != null && !hasBeenTaught.isEmpty();
			case CoursePagePackage.COURSE__PLANNED_FOR:
				return plannedFor != null && !plannedFor.isEmpty();
			case CoursePagePackage.COURSE__SEMESTER:
				return SEMESTER_EDEFAULT == null ? semester != null : !SEMESTER_EDEFAULT.equals(semester);
			case CoursePagePackage.COURSE__COURSE_STAFF:
				return courseStaff != null && !courseStaff.isEmpty();
			case CoursePagePackage.COURSE__BELONGS_TO:
				return getBelongsTo() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (code: ");
		result.append(code);
		result.append(", name: ");
		result.append(name);
		result.append(", content: ");
		result.append(content);
		result.append(", credits: ");
		result.append(credits);
		result.append(", semester: ");
		result.append(semester);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
