/**
 */
package coursePage.impl;

import coursePage.CoursePagePackage;
import coursePage.Day;
import coursePage.LectureType;
import coursePage.Room;
import coursePage.Slot;
import coursePage.StudyProgram;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Slot</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coursePage.impl.SlotImpl#getDay <em>Day</em>}</li>
 *   <li>{@link coursePage.impl.SlotImpl#getRoom <em>Room</em>}</li>
 *   <li>{@link coursePage.impl.SlotImpl#getLectureType <em>Lecture Type</em>}</li>
 *   <li>{@link coursePage.impl.SlotImpl#getStarteTime <em>Starte Time</em>}</li>
 *   <li>{@link coursePage.impl.SlotImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link coursePage.impl.SlotImpl#getScheduleFor <em>Schedule For</em>}</li>
 *   <li>{@link coursePage.impl.SlotImpl#getHours <em>Hours</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SlotImpl extends MinimalEObjectImpl.Container implements Slot {
	/**
	 * The default value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected static final Day DAY_EDEFAULT = Day.MONDAY;

	/**
	 * The cached value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected Day day = DAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoom() <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected static final Room ROOM_EDEFAULT = Room.F1;

	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected Room room = ROOM_EDEFAULT;

	/**
	 * The default value of the '{@link #getLectureType() <em>Lecture Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureType()
	 * @generated
	 * @ordered
	 */
	protected static final LectureType LECTURE_TYPE_EDEFAULT = LectureType.LAB;

	/**
	 * The cached value of the '{@link #getLectureType() <em>Lecture Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureType()
	 * @generated
	 * @ordered
	 */
	protected LectureType lectureType = LECTURE_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStarteTime() <em>Starte Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStarteTime()
	 * @generated
	 * @ordered
	 */
	protected static final String STARTE_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStarteTime() <em>Starte Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStarteTime()
	 * @generated
	 * @ordered
	 */
	protected String starteTime = STARTE_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final String END_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected String endTime = END_TIME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getScheduleFor() <em>Schedule For</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScheduleFor()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> scheduleFor;

	/**
	 * The default value of the '{@link #getHours() <em>Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHours()
	 * @generated
	 * @ordered
	 */
	protected static final double HOURS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getHours() <em>Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHours()
	 * @generated
	 * @ordered
	 */
	protected double hours = HOURS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SlotImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePagePackage.Literals.SLOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Day getDay() {
		return day;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDay(Day newDay) {
		Day oldDay = day;
		day = newDay == null ? DAY_EDEFAULT : newDay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.SLOT__DAY, oldDay, day));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room getRoom() {
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoom(Room newRoom) {
		Room oldRoom = room;
		room = newRoom == null ? ROOM_EDEFAULT : newRoom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.SLOT__ROOM, oldRoom, room));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LectureType getLectureType() {
		return lectureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLectureType(LectureType newLectureType) {
		LectureType oldLectureType = lectureType;
		lectureType = newLectureType == null ? LECTURE_TYPE_EDEFAULT : newLectureType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.SLOT__LECTURE_TYPE, oldLectureType, lectureType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStarteTime() {
		return starteTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStarteTime(String newStarteTime) {
		String oldStarteTime = starteTime;
		starteTime = newStarteTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.SLOT__STARTE_TIME, oldStarteTime, starteTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(String newEndTime) {
		String oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.SLOT__END_TIME, oldEndTime, endTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getScheduleFor() {
		if (scheduleFor == null) {
			scheduleFor = new EObjectResolvingEList<StudyProgram>(StudyProgram.class, this, CoursePagePackage.SLOT__SCHEDULE_FOR);
		}
		return scheduleFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getHours() {
		return hours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHours(double newHours) {
		double oldHours = hours;
		hours = newHours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.SLOT__HOURS, oldHours, hours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePagePackage.SLOT__DAY:
				return getDay();
			case CoursePagePackage.SLOT__ROOM:
				return getRoom();
			case CoursePagePackage.SLOT__LECTURE_TYPE:
				return getLectureType();
			case CoursePagePackage.SLOT__STARTE_TIME:
				return getStarteTime();
			case CoursePagePackage.SLOT__END_TIME:
				return getEndTime();
			case CoursePagePackage.SLOT__SCHEDULE_FOR:
				return getScheduleFor();
			case CoursePagePackage.SLOT__HOURS:
				return getHours();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePagePackage.SLOT__DAY:
				setDay((Day)newValue);
				return;
			case CoursePagePackage.SLOT__ROOM:
				setRoom((Room)newValue);
				return;
			case CoursePagePackage.SLOT__LECTURE_TYPE:
				setLectureType((LectureType)newValue);
				return;
			case CoursePagePackage.SLOT__STARTE_TIME:
				setStarteTime((String)newValue);
				return;
			case CoursePagePackage.SLOT__END_TIME:
				setEndTime((String)newValue);
				return;
			case CoursePagePackage.SLOT__SCHEDULE_FOR:
				getScheduleFor().clear();
				getScheduleFor().addAll((Collection<? extends StudyProgram>)newValue);
				return;
			case CoursePagePackage.SLOT__HOURS:
				setHours((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePagePackage.SLOT__DAY:
				setDay(DAY_EDEFAULT);
				return;
			case CoursePagePackage.SLOT__ROOM:
				setRoom(ROOM_EDEFAULT);
				return;
			case CoursePagePackage.SLOT__LECTURE_TYPE:
				setLectureType(LECTURE_TYPE_EDEFAULT);
				return;
			case CoursePagePackage.SLOT__STARTE_TIME:
				setStarteTime(STARTE_TIME_EDEFAULT);
				return;
			case CoursePagePackage.SLOT__END_TIME:
				setEndTime(END_TIME_EDEFAULT);
				return;
			case CoursePagePackage.SLOT__SCHEDULE_FOR:
				getScheduleFor().clear();
				return;
			case CoursePagePackage.SLOT__HOURS:
				setHours(HOURS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePagePackage.SLOT__DAY:
				return day != DAY_EDEFAULT;
			case CoursePagePackage.SLOT__ROOM:
				return room != ROOM_EDEFAULT;
			case CoursePagePackage.SLOT__LECTURE_TYPE:
				return lectureType != LECTURE_TYPE_EDEFAULT;
			case CoursePagePackage.SLOT__STARTE_TIME:
				return STARTE_TIME_EDEFAULT == null ? starteTime != null : !STARTE_TIME_EDEFAULT.equals(starteTime);
			case CoursePagePackage.SLOT__END_TIME:
				return END_TIME_EDEFAULT == null ? endTime != null : !END_TIME_EDEFAULT.equals(endTime);
			case CoursePagePackage.SLOT__SCHEDULE_FOR:
				return scheduleFor != null && !scheduleFor.isEmpty();
			case CoursePagePackage.SLOT__HOURS:
				return hours != HOURS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (day: ");
		result.append(day);
		result.append(", room: ");
		result.append(room);
		result.append(", lectureType: ");
		result.append(lectureType);
		result.append(", starteTime: ");
		result.append(starteTime);
		result.append(", endTime: ");
		result.append(endTime);
		result.append(", hours: ");
		result.append(hours);
		result.append(')');
		return result.toString();
	}

} //SlotImpl
