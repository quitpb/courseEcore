/**
 */
package coursePage.impl;

import coursePage.Course;
import coursePage.CoursePagePackage;
import coursePage.Department;
import coursePage.StudyCode;
import coursePage.StudyProgram;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Study Program</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coursePage.impl.StudyProgramImpl#getCode <em>Code</em>}</li>
 *   <li>{@link coursePage.impl.StudyProgramImpl#getCanSignUpFor <em>Can Sign Up For</em>}</li>
 *   <li>{@link coursePage.impl.StudyProgramImpl#getBelongsTo <em>Belongs To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StudyProgramImpl extends MinimalEObjectImpl.Container implements StudyProgram {
	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final StudyCode CODE_EDEFAULT = StudyCode.MTKYB;
	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected StudyCode code = CODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCanSignUpFor() <em>Can Sign Up For</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCanSignUpFor()
	 * @generated
	 * @ordered
	 */
	protected Course canSignUpFor;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyProgramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePagePackage.Literals.STUDY_PROGRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyCode getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(StudyCode newCode) {
		StudyCode oldCode = code;
		code = newCode == null ? CODE_EDEFAULT : newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.STUDY_PROGRAM__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getCanSignUpFor() {
		if (canSignUpFor != null && canSignUpFor.eIsProxy()) {
			InternalEObject oldCanSignUpFor = (InternalEObject)canSignUpFor;
			canSignUpFor = (Course)eResolveProxy(oldCanSignUpFor);
			if (canSignUpFor != oldCanSignUpFor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePagePackage.STUDY_PROGRAM__CAN_SIGN_UP_FOR, oldCanSignUpFor, canSignUpFor));
			}
		}
		return canSignUpFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course basicGetCanSignUpFor() {
		return canSignUpFor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCanSignUpFor(Course newCanSignUpFor, NotificationChain msgs) {
		Course oldCanSignUpFor = canSignUpFor;
		canSignUpFor = newCanSignUpFor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoursePagePackage.STUDY_PROGRAM__CAN_SIGN_UP_FOR, oldCanSignUpFor, newCanSignUpFor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCanSignUpFor(Course newCanSignUpFor) {
		if (newCanSignUpFor != canSignUpFor) {
			NotificationChain msgs = null;
			if (canSignUpFor != null)
				msgs = ((InternalEObject)canSignUpFor).eInverseRemove(this, CoursePagePackage.COURSE__PLANNED_FOR, Course.class, msgs);
			if (newCanSignUpFor != null)
				msgs = ((InternalEObject)newCanSignUpFor).eInverseAdd(this, CoursePagePackage.COURSE__PLANNED_FOR, Course.class, msgs);
			msgs = basicSetCanSignUpFor(newCanSignUpFor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.STUDY_PROGRAM__CAN_SIGN_UP_FOR, newCanSignUpFor, newCanSignUpFor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getBelongsTo() {
		if (eContainerFeatureID() != CoursePagePackage.STUDY_PROGRAM__BELONGS_TO) return null;
		return (Department)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBelongsTo(Department newBelongsTo, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newBelongsTo, CoursePagePackage.STUDY_PROGRAM__BELONGS_TO, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBelongsTo(Department newBelongsTo) {
		if (newBelongsTo != eInternalContainer() || (eContainerFeatureID() != CoursePagePackage.STUDY_PROGRAM__BELONGS_TO && newBelongsTo != null)) {
			if (EcoreUtil.isAncestor(this, newBelongsTo))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newBelongsTo != null)
				msgs = ((InternalEObject)newBelongsTo).eInverseAdd(this, CoursePagePackage.DEPARTMENT__RESPONSIBLE_FOR, Department.class, msgs);
			msgs = basicSetBelongsTo(newBelongsTo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.STUDY_PROGRAM__BELONGS_TO, newBelongsTo, newBelongsTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.STUDY_PROGRAM__CAN_SIGN_UP_FOR:
				if (canSignUpFor != null)
					msgs = ((InternalEObject)canSignUpFor).eInverseRemove(this, CoursePagePackage.COURSE__PLANNED_FOR, Course.class, msgs);
				return basicSetCanSignUpFor((Course)otherEnd, msgs);
			case CoursePagePackage.STUDY_PROGRAM__BELONGS_TO:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetBelongsTo((Department)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.STUDY_PROGRAM__CAN_SIGN_UP_FOR:
				return basicSetCanSignUpFor(null, msgs);
			case CoursePagePackage.STUDY_PROGRAM__BELONGS_TO:
				return basicSetBelongsTo(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CoursePagePackage.STUDY_PROGRAM__BELONGS_TO:
				return eInternalContainer().eInverseRemove(this, CoursePagePackage.DEPARTMENT__RESPONSIBLE_FOR, Department.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePagePackage.STUDY_PROGRAM__CODE:
				return getCode();
			case CoursePagePackage.STUDY_PROGRAM__CAN_SIGN_UP_FOR:
				if (resolve) return getCanSignUpFor();
				return basicGetCanSignUpFor();
			case CoursePagePackage.STUDY_PROGRAM__BELONGS_TO:
				return getBelongsTo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePagePackage.STUDY_PROGRAM__CODE:
				setCode((StudyCode)newValue);
				return;
			case CoursePagePackage.STUDY_PROGRAM__CAN_SIGN_UP_FOR:
				setCanSignUpFor((Course)newValue);
				return;
			case CoursePagePackage.STUDY_PROGRAM__BELONGS_TO:
				setBelongsTo((Department)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePagePackage.STUDY_PROGRAM__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case CoursePagePackage.STUDY_PROGRAM__CAN_SIGN_UP_FOR:
				setCanSignUpFor((Course)null);
				return;
			case CoursePagePackage.STUDY_PROGRAM__BELONGS_TO:
				setBelongsTo((Department)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePagePackage.STUDY_PROGRAM__CODE:
				return code != CODE_EDEFAULT;
			case CoursePagePackage.STUDY_PROGRAM__CAN_SIGN_UP_FOR:
				return canSignUpFor != null;
			case CoursePagePackage.STUDY_PROGRAM__BELONGS_TO:
				return getBelongsTo() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (code: ");
		result.append(code);
		result.append(')');
		return result.toString();
	}

} //StudyProgramImpl
