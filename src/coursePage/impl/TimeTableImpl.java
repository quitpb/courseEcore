/**
 */
package coursePage.impl;

import coursePage.Course;
import coursePage.CoursePagePackage;
import coursePage.Slot;
import coursePage.TimeTable;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coursePage.impl.TimeTableImpl#getTable <em>Table</em>}</li>
 *   <li>{@link coursePage.impl.TimeTableImpl#getFor <em>For</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimeTableImpl extends MinimalEObjectImpl.Container implements TimeTable {
	/**
	 * The cached value of the '{@link #getTable() <em>Table</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTable()
	 * @generated
	 * @ordered
	 */
	protected EList<Slot> table;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeTableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePagePackage.Literals.TIME_TABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Slot> getTable() {
		if (table == null) {
			table = new EObjectContainmentEList<Slot>(Slot.class, this, CoursePagePackage.TIME_TABLE__TABLE);
		}
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getFor() {
		if (eContainerFeatureID() != CoursePagePackage.TIME_TABLE__FOR) return null;
		return (Course)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFor(Course newFor, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newFor, CoursePagePackage.TIME_TABLE__FOR, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFor(Course newFor) {
		if (newFor != eInternalContainer() || (eContainerFeatureID() != CoursePagePackage.TIME_TABLE__FOR && newFor != null)) {
			if (EcoreUtil.isAncestor(this, newFor))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newFor != null)
				msgs = ((InternalEObject)newFor).eInverseAdd(this, CoursePagePackage.COURSE__TIMETABLE, Course.class, msgs);
			msgs = basicSetFor(newFor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.TIME_TABLE__FOR, newFor, newFor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.TIME_TABLE__FOR:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetFor((Course)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.TIME_TABLE__TABLE:
				return ((InternalEList<?>)getTable()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.TIME_TABLE__FOR:
				return basicSetFor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CoursePagePackage.TIME_TABLE__FOR:
				return eInternalContainer().eInverseRemove(this, CoursePagePackage.COURSE__TIMETABLE, Course.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePagePackage.TIME_TABLE__TABLE:
				return getTable();
			case CoursePagePackage.TIME_TABLE__FOR:
				return getFor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePagePackage.TIME_TABLE__TABLE:
				getTable().clear();
				getTable().addAll((Collection<? extends Slot>)newValue);
				return;
			case CoursePagePackage.TIME_TABLE__FOR:
				setFor((Course)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePagePackage.TIME_TABLE__TABLE:
				getTable().clear();
				return;
			case CoursePagePackage.TIME_TABLE__FOR:
				setFor((Course)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePagePackage.TIME_TABLE__TABLE:
				return table != null && !table.isEmpty();
			case CoursePagePackage.TIME_TABLE__FOR:
				return getFor() != null;
		}
		return super.eIsSet(featureID);
	}

} //TimeTableImpl
