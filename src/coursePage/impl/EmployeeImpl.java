/**
 */
package coursePage.impl;

import coursePage.CourseCordinator;
import coursePage.CoursePagePackage;
import coursePage.Department;
import coursePage.Employee;

import coursePage.Lecturer;
import coursePage.Staff;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Employee</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coursePage.impl.EmployeeImpl#getName <em>Name</em>}</li>
 *   <li>{@link coursePage.impl.EmployeeImpl#getWorksFor <em>Works For</em>}</li>
 *   <li>{@link coursePage.impl.EmployeeImpl#getRole <em>Role</em>}</li>
 *   <li>{@link coursePage.impl.EmployeeImpl#getIsCC <em>Is CC</em>}</li>
 *   <li>{@link coursePage.impl.EmployeeImpl#getInThis <em>In This</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EmployeeImpl extends MinimalEObjectImpl.Container implements Employee {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRole() <em>Role</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected EList<Lecturer> role;

	/**
	 * The cached value of the '{@link #getIsCC() <em>Is CC</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsCC()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseCordinator> isCC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EmployeeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePagePackage.Literals.EMPLOYEE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.EMPLOYEE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getWorksFor() {
		if (eContainerFeatureID() != CoursePagePackage.EMPLOYEE__WORKS_FOR) return null;
		return (Department)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWorksFor(Department newWorksFor, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newWorksFor, CoursePagePackage.EMPLOYEE__WORKS_FOR, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorksFor(Department newWorksFor) {
		if (newWorksFor != eInternalContainer() || (eContainerFeatureID() != CoursePagePackage.EMPLOYEE__WORKS_FOR && newWorksFor != null)) {
			if (EcoreUtil.isAncestor(this, newWorksFor))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newWorksFor != null)
				msgs = ((InternalEObject)newWorksFor).eInverseAdd(this, CoursePagePackage.DEPARTMENT__HAS, Department.class, msgs);
			msgs = basicSetWorksFor(newWorksFor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.EMPLOYEE__WORKS_FOR, newWorksFor, newWorksFor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Lecturer> getRole() {
		if (role == null) {
			role = new EObjectContainmentEList<Lecturer>(Lecturer.class, this, CoursePagePackage.EMPLOYEE__ROLE);
		}
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseCordinator> getIsCC() {
		if (isCC == null) {
			isCC = new EObjectContainmentEList<CourseCordinator>(CourseCordinator.class, this, CoursePagePackage.EMPLOYEE__IS_CC);
		}
		return isCC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Staff getInThis() {
		if (eContainerFeatureID() != CoursePagePackage.EMPLOYEE__IN_THIS) return null;
		return (Staff)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInThis(Staff newInThis, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newInThis, CoursePagePackage.EMPLOYEE__IN_THIS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInThis(Staff newInThis) {
		if (newInThis != eInternalContainer() || (eContainerFeatureID() != CoursePagePackage.EMPLOYEE__IN_THIS && newInThis != null)) {
			if (EcoreUtil.isAncestor(this, newInThis))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newInThis != null)
				msgs = ((InternalEObject)newInThis).eInverseAdd(this, CoursePagePackage.STAFF__IN_THE_STAFF, Staff.class, msgs);
			msgs = basicSetInThis(newInThis, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.EMPLOYEE__IN_THIS, newInThis, newInThis));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.EMPLOYEE__WORKS_FOR:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetWorksFor((Department)otherEnd, msgs);
			case CoursePagePackage.EMPLOYEE__IN_THIS:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetInThis((Staff)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.EMPLOYEE__WORKS_FOR:
				return basicSetWorksFor(null, msgs);
			case CoursePagePackage.EMPLOYEE__ROLE:
				return ((InternalEList<?>)getRole()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.EMPLOYEE__IS_CC:
				return ((InternalEList<?>)getIsCC()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.EMPLOYEE__IN_THIS:
				return basicSetInThis(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CoursePagePackage.EMPLOYEE__WORKS_FOR:
				return eInternalContainer().eInverseRemove(this, CoursePagePackage.DEPARTMENT__HAS, Department.class, msgs);
			case CoursePagePackage.EMPLOYEE__IN_THIS:
				return eInternalContainer().eInverseRemove(this, CoursePagePackage.STAFF__IN_THE_STAFF, Staff.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePagePackage.EMPLOYEE__NAME:
				return getName();
			case CoursePagePackage.EMPLOYEE__WORKS_FOR:
				return getWorksFor();
			case CoursePagePackage.EMPLOYEE__ROLE:
				return getRole();
			case CoursePagePackage.EMPLOYEE__IS_CC:
				return getIsCC();
			case CoursePagePackage.EMPLOYEE__IN_THIS:
				return getInThis();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePagePackage.EMPLOYEE__NAME:
				setName((String)newValue);
				return;
			case CoursePagePackage.EMPLOYEE__WORKS_FOR:
				setWorksFor((Department)newValue);
				return;
			case CoursePagePackage.EMPLOYEE__ROLE:
				getRole().clear();
				getRole().addAll((Collection<? extends Lecturer>)newValue);
				return;
			case CoursePagePackage.EMPLOYEE__IS_CC:
				getIsCC().clear();
				getIsCC().addAll((Collection<? extends CourseCordinator>)newValue);
				return;
			case CoursePagePackage.EMPLOYEE__IN_THIS:
				setInThis((Staff)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePagePackage.EMPLOYEE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CoursePagePackage.EMPLOYEE__WORKS_FOR:
				setWorksFor((Department)null);
				return;
			case CoursePagePackage.EMPLOYEE__ROLE:
				getRole().clear();
				return;
			case CoursePagePackage.EMPLOYEE__IS_CC:
				getIsCC().clear();
				return;
			case CoursePagePackage.EMPLOYEE__IN_THIS:
				setInThis((Staff)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePagePackage.EMPLOYEE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CoursePagePackage.EMPLOYEE__WORKS_FOR:
				return getWorksFor() != null;
			case CoursePagePackage.EMPLOYEE__ROLE:
				return role != null && !role.isEmpty();
			case CoursePagePackage.EMPLOYEE__IS_CC:
				return isCC != null && !isCC.isEmpty();
			case CoursePagePackage.EMPLOYEE__IN_THIS:
				return getInThis() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //EmployeeImpl
