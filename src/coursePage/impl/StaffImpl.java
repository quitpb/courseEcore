/**
 */
package coursePage.impl;

import coursePage.Course;
import coursePage.CoursePagePackage;
import coursePage.Employee;
import coursePage.Staff;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Staff</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link coursePage.impl.StaffImpl#getInTheStaff <em>In The Staff</em>}</li>
 *   <li>{@link coursePage.impl.StaffImpl#getWorksFor <em>Works For</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StaffImpl extends MinimalEObjectImpl.Container implements Staff {
	/**
	 * The cached value of the '{@link #getInTheStaff() <em>In The Staff</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInTheStaff()
	 * @generated
	 * @ordered
	 */
	protected EList<Employee> inTheStaff;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StaffImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePagePackage.Literals.STAFF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Employee> getInTheStaff() {
		if (inTheStaff == null) {
			inTheStaff = new EObjectContainmentWithInverseEList<Employee>(Employee.class, this, CoursePagePackage.STAFF__IN_THE_STAFF, CoursePagePackage.EMPLOYEE__IN_THIS);
		}
		return inTheStaff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getWorksFor() {
		if (eContainerFeatureID() != CoursePagePackage.STAFF__WORKS_FOR) return null;
		return (Course)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWorksFor(Course newWorksFor, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newWorksFor, CoursePagePackage.STAFF__WORKS_FOR, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorksFor(Course newWorksFor) {
		if (newWorksFor != eInternalContainer() || (eContainerFeatureID() != CoursePagePackage.STAFF__WORKS_FOR && newWorksFor != null)) {
			if (EcoreUtil.isAncestor(this, newWorksFor))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newWorksFor != null)
				msgs = ((InternalEObject)newWorksFor).eInverseAdd(this, CoursePagePackage.COURSE__COURSE_STAFF, Course.class, msgs);
			msgs = basicSetWorksFor(newWorksFor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePagePackage.STAFF__WORKS_FOR, newWorksFor, newWorksFor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.STAFF__IN_THE_STAFF:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInTheStaff()).basicAdd(otherEnd, msgs);
			case CoursePagePackage.STAFF__WORKS_FOR:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetWorksFor((Course)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoursePagePackage.STAFF__IN_THE_STAFF:
				return ((InternalEList<?>)getInTheStaff()).basicRemove(otherEnd, msgs);
			case CoursePagePackage.STAFF__WORKS_FOR:
				return basicSetWorksFor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CoursePagePackage.STAFF__WORKS_FOR:
				return eInternalContainer().eInverseRemove(this, CoursePagePackage.COURSE__COURSE_STAFF, Course.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoursePagePackage.STAFF__IN_THE_STAFF:
				return getInTheStaff();
			case CoursePagePackage.STAFF__WORKS_FOR:
				return getWorksFor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoursePagePackage.STAFF__IN_THE_STAFF:
				getInTheStaff().clear();
				getInTheStaff().addAll((Collection<? extends Employee>)newValue);
				return;
			case CoursePagePackage.STAFF__WORKS_FOR:
				setWorksFor((Course)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoursePagePackage.STAFF__IN_THE_STAFF:
				getInTheStaff().clear();
				return;
			case CoursePagePackage.STAFF__WORKS_FOR:
				setWorksFor((Course)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoursePagePackage.STAFF__IN_THE_STAFF:
				return inTheStaff != null && !inTheStaff.isEmpty();
			case CoursePagePackage.STAFF__WORKS_FOR:
				return getWorksFor() != null;
		}
		return super.eIsSet(featureID);
	}

} //StaffImpl
