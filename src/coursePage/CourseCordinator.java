/**
 */
package coursePage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Cordinator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see coursePage.CoursePagePackage#getCourseCordinator()
 * @model
 * @generated
 */
public interface CourseCordinator extends EObject {
} // CourseCordinator
