/**
 */
package coursePage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see coursePage.CoursePageFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/acceleo/query/1.0'"
 * @generated
 */
public interface CoursePagePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "coursePage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/tdt4250.web/model/coursePage.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "coursePage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoursePagePackage eINSTANCE = coursePage.impl.CoursePagePackageImpl.init();

	/**
	 * The meta object id for the '{@link coursePage.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.CourseImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CONTENT = 2;

	/**
	 * The feature id for the '<em><b>Timetable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__TIMETABLE = 3;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__RELATION = 4;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS = 5;

	/**
	 * The feature id for the '<em><b>Grading</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__GRADING = 6;

	/**
	 * The feature id for the '<em><b>Required</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__REQUIRED = 7;

	/**
	 * The feature id for the '<em><b>Course Work</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSE_WORK = 8;

	/**
	 * The feature id for the '<em><b>Has Been Taught</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__HAS_BEEN_TAUGHT = 9;

	/**
	 * The feature id for the '<em><b>Planned For</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__PLANNED_FOR = 10;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__SEMESTER = 11;

	/**
	 * The feature id for the '<em><b>Course Staff</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSE_STAFF = 12;

	/**
	 * The feature id for the '<em><b>Belongs To</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__BELONGS_TO = 13;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 14;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.DepartmentImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Has</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__HAS = 1;

	/**
	 * The feature id for the '<em><b>Responsible Forr</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__RESPONSIBLE_FORR = 2;

	/**
	 * The feature id for the '<em><b>Responsible For</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__RESPONSIBLE_FOR = 3;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.EvaluationForm <em>Evaluation Form</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.EvaluationForm
	 * @see coursePage.impl.CoursePagePackageImpl#getEvaluationForm()
	 * @generated
	 */
	int EVALUATION_FORM = 14;

	/**
	 * The meta object id for the '{@link coursePage.RelatedCourse <em>Related Course</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.RelatedCourse
	 * @see coursePage.impl.CoursePagePackageImpl#getRelatedCourse()
	 * @generated
	 */
	int RELATED_COURSE = 15;

	/**
	 * The meta object id for the '{@link coursePage.LectureType <em>Lecture Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.LectureType
	 * @see coursePage.impl.CoursePagePackageImpl#getLectureType()
	 * @generated
	 */
	int LECTURE_TYPE = 16;

	/**
	 * The meta object id for the '{@link coursePage.Day <em>Day</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.Day
	 * @see coursePage.impl.CoursePagePackageImpl#getDay()
	 * @generated
	 */
	int DAY = 17;

	/**
	 * The meta object id for the '{@link coursePage.Room <em>Room</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.Room
	 * @see coursePage.impl.CoursePagePackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 18;

	/**
	 * The meta object id for the '{@link coursePage.StudyCode <em>Study Code</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.StudyCode
	 * @see coursePage.impl.CoursePagePackageImpl#getStudyCode()
	 * @generated
	 */
	int STUDY_CODE = 19;

	/**
	 * The meta object id for the '{@link coursePage.impl.TimeTableImpl <em>Time Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.TimeTableImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getTimeTable()
	 * @generated
	 */
	int TIME_TABLE = 2;

	/**
	 * The feature id for the '<em><b>Table</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_TABLE__TABLE = 0;

	/**
	 * The feature id for the '<em><b>For</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_TABLE__FOR = 1;

	/**
	 * The number of structural features of the '<em>Time Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_TABLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Time Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_TABLE_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link coursePage.impl.EmployeeImpl <em>Employee</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.EmployeeImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getEmployee()
	 * @generated
	 */
	int EMPLOYEE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPLOYEE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Works For</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPLOYEE__WORKS_FOR = 1;

	/**
	 * The feature id for the '<em><b>Role</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPLOYEE__ROLE = 2;

	/**
	 * The feature id for the '<em><b>Is CC</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPLOYEE__IS_CC = 3;

	/**
	 * The feature id for the '<em><b>In This</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPLOYEE__IN_THIS = 4;

	/**
	 * The number of structural features of the '<em>Employee</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPLOYEE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Employee</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPLOYEE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.impl.RelationToCourseImpl <em>Relation To Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.RelationToCourseImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getRelationToCourse()
	 * @generated
	 */
	int RELATION_TO_COURSE = 4;

	/**
	 * The feature id for the '<em><b>Course</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_TO_COURSE__COURSE = 0;

	/**
	 * The feature id for the '<em><b>Credits Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_TO_COURSE__CREDITS_REDUCTION = 1;

	/**
	 * The number of structural features of the '<em>Relation To Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_TO_COURSE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Relation To Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_TO_COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.impl.StudyProgramImpl <em>Study Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.StudyProgramImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getStudyProgram()
	 * @generated
	 */
	int STUDY_PROGRAM = 5;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__CODE = 0;

	/**
	 * The feature id for the '<em><b>Can Sign Up For</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__CAN_SIGN_UP_FOR = 1;

	/**
	 * The feature id for the '<em><b>Belongs To</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__BELONGS_TO = 2;

	/**
	 * The number of structural features of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link coursePage.impl.SlotImpl <em>Slot</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.SlotImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getSlot()
	 * @generated
	 */
	int SLOT = 6;

	/**
	 * The feature id for the '<em><b>Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLOT__DAY = 0;

	/**
	 * The feature id for the '<em><b>Room</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLOT__ROOM = 1;

	/**
	 * The feature id for the '<em><b>Lecture Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLOT__LECTURE_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Starte Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLOT__STARTE_TIME = 3;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLOT__END_TIME = 4;

	/**
	 * The feature id for the '<em><b>Schedule For</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLOT__SCHEDULE_FOR = 5;

	/**
	 * The feature id for the '<em><b>Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLOT__HOURS = 6;

	/**
	 * The number of structural features of the '<em>Slot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLOT_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Slot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.impl.EvaluationImpl <em>Evaluation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.EvaluationImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getEvaluation()
	 * @generated
	 */
	int EVALUATION = 7;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION__EVALUATION = 0;

	/**
	 * The feature id for the '<em><b>Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION__POINTS = 1;

	/**
	 * The number of structural features of the '<em>Evaluation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Evaluation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.impl.CourseWorkImpl <em>Course Work</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.CourseWorkImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getCourseWork()
	 * @generated
	 */
	int COURSE_WORK = 8;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__LECTURE_HOURS = 0;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK__LAB_HOURS = 1;

	/**
	 * The number of structural features of the '<em>Course Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Course Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_WORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.CourseInstanceImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getCourseInstance()
	 * @generated
	 */
	int COURSE_INSTANCE = 9;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__SEMESTER = 0;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__CREDITS = 1;

	/**
	 * The number of structural features of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.impl.LecturerImpl <em>Lecturer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.LecturerImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getLecturer()
	 * @generated
	 */
	int LECTURER = 10;

	/**
	 * The feature id for the '<em><b>Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURER__ROLE = 0;

	/**
	 * The number of structural features of the '<em>Lecturer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Lecturer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.impl.CourseCordinatorImpl <em>Course Cordinator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.CourseCordinatorImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getCourseCordinator()
	 * @generated
	 */
	int COURSE_CORDINATOR = 11;

	/**
	 * The number of structural features of the '<em>Course Cordinator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_CORDINATOR_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Course Cordinator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_CORDINATOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.impl.StaffImpl <em>Staff</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.impl.StaffImpl
	 * @see coursePage.impl.CoursePagePackageImpl#getStaff()
	 * @generated
	 */
	int STAFF = 12;

	/**
	 * The feature id for the '<em><b>In The Staff</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF__IN_THE_STAFF = 0;

	/**
	 * The feature id for the '<em><b>Works For</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF__WORKS_FOR = 1;

	/**
	 * The number of structural features of the '<em>Staff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Staff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAFF_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link coursePage.Role <em>Role</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see coursePage.Role
	 * @see coursePage.impl.CoursePagePackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 13;


	/**
	 * Returns the meta object for class '{@link coursePage.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see coursePage.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see coursePage.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see coursePage.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Course#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see coursePage.Course#getContent()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Content();

	/**
	 * Returns the meta object for the containment reference '{@link coursePage.Course#getTimetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timetable</em>'.
	 * @see coursePage.Course#getTimetable()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Timetable();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.Course#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relation</em>'.
	 * @see coursePage.Course#getRelation()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Relation();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Course#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see coursePage.Course#getCredits()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Credits();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.Course#getGrading <em>Grading</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Grading</em>'.
	 * @see coursePage.Course#getGrading()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Grading();

	/**
	 * Returns the meta object for the reference '{@link coursePage.Course#getRequired <em>Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required</em>'.
	 * @see coursePage.Course#getRequired()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Required();

	/**
	 * Returns the meta object for the containment reference '{@link coursePage.Course#getCourseWork <em>Course Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Course Work</em>'.
	 * @see coursePage.Course#getCourseWork()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CourseWork();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.Course#getHasBeenTaught <em>Has Been Taught</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Been Taught</em>'.
	 * @see coursePage.Course#getHasBeenTaught()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_HasBeenTaught();

	/**
	 * Returns the meta object for the reference list '{@link coursePage.Course#getPlannedFor <em>Planned For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Planned For</em>'.
	 * @see coursePage.Course#getPlannedFor()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_PlannedFor();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Course#getSemester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester</em>'.
	 * @see coursePage.Course#getSemester()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Semester();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.Course#getCourseStaff <em>Course Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Course Staff</em>'.
	 * @see coursePage.Course#getCourseStaff()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CourseStaff();

	/**
	 * Returns the meta object for the container reference '{@link coursePage.Course#getBelongsTo <em>Belongs To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Belongs To</em>'.
	 * @see coursePage.Course#getBelongsTo()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_BelongsTo();

	/**
	 * Returns the meta object for class '{@link coursePage.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see coursePage.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Department#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see coursePage.Department#getName()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.Department#getHas <em>Has</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has</em>'.
	 * @see coursePage.Department#getHas()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Has();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.Department#getResponsibleForr <em>Responsible Forr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Responsible Forr</em>'.
	 * @see coursePage.Department#getResponsibleForr()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_ResponsibleForr();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.Department#getResponsibleFor <em>Responsible For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Responsible For</em>'.
	 * @see coursePage.Department#getResponsibleFor()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_ResponsibleFor();

	/**
	 * Returns the meta object for enum '{@link coursePage.EvaluationForm <em>Evaluation Form</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Evaluation Form</em>'.
	 * @see coursePage.EvaluationForm
	 * @generated
	 */
	EEnum getEvaluationForm();

	/**
	 * Returns the meta object for enum '{@link coursePage.RelatedCourse <em>Related Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Related Course</em>'.
	 * @see coursePage.RelatedCourse
	 * @generated
	 */
	EEnum getRelatedCourse();

	/**
	 * Returns the meta object for enum '{@link coursePage.LectureType <em>Lecture Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Lecture Type</em>'.
	 * @see coursePage.LectureType
	 * @generated
	 */
	EEnum getLectureType();

	/**
	 * Returns the meta object for enum '{@link coursePage.Day <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Day</em>'.
	 * @see coursePage.Day
	 * @generated
	 */
	EEnum getDay();

	/**
	 * Returns the meta object for enum '{@link coursePage.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Room</em>'.
	 * @see coursePage.Room
	 * @generated
	 */
	EEnum getRoom();

	/**
	 * Returns the meta object for enum '{@link coursePage.StudyCode <em>Study Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Study Code</em>'.
	 * @see coursePage.StudyCode
	 * @generated
	 */
	EEnum getStudyCode();

	/**
	 * Returns the meta object for class '{@link coursePage.TimeTable <em>Time Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Table</em>'.
	 * @see coursePage.TimeTable
	 * @generated
	 */
	EClass getTimeTable();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.TimeTable#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Table</em>'.
	 * @see coursePage.TimeTable#getTable()
	 * @see #getTimeTable()
	 * @generated
	 */
	EReference getTimeTable_Table();

	/**
	 * Returns the meta object for the container reference '{@link coursePage.TimeTable#getFor <em>For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>For</em>'.
	 * @see coursePage.TimeTable#getFor()
	 * @see #getTimeTable()
	 * @generated
	 */
	EReference getTimeTable_For();

	/**
	 * Returns the meta object for class '{@link coursePage.Employee <em>Employee</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Employee</em>'.
	 * @see coursePage.Employee
	 * @generated
	 */
	EClass getEmployee();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Employee#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see coursePage.Employee#getName()
	 * @see #getEmployee()
	 * @generated
	 */
	EAttribute getEmployee_Name();

	/**
	 * Returns the meta object for the container reference '{@link coursePage.Employee#getWorksFor <em>Works For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Works For</em>'.
	 * @see coursePage.Employee#getWorksFor()
	 * @see #getEmployee()
	 * @generated
	 */
	EReference getEmployee_WorksFor();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.Employee#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role</em>'.
	 * @see coursePage.Employee#getRole()
	 * @see #getEmployee()
	 * @generated
	 */
	EReference getEmployee_Role();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.Employee#getIsCC <em>Is CC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Is CC</em>'.
	 * @see coursePage.Employee#getIsCC()
	 * @see #getEmployee()
	 * @generated
	 */
	EReference getEmployee_IsCC();

	/**
	 * Returns the meta object for the container reference '{@link coursePage.Employee#getInThis <em>In This</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>In This</em>'.
	 * @see coursePage.Employee#getInThis()
	 * @see #getEmployee()
	 * @generated
	 */
	EReference getEmployee_InThis();

	/**
	 * Returns the meta object for class '{@link coursePage.RelationToCourse <em>Relation To Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation To Course</em>'.
	 * @see coursePage.RelationToCourse
	 * @generated
	 */
	EClass getRelationToCourse();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.RelationToCourse#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Course</em>'.
	 * @see coursePage.RelationToCourse#getCourse()
	 * @see #getRelationToCourse()
	 * @generated
	 */
	EAttribute getRelationToCourse_Course();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.RelationToCourse#getCreditsReduction <em>Credits Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits Reduction</em>'.
	 * @see coursePage.RelationToCourse#getCreditsReduction()
	 * @see #getRelationToCourse()
	 * @generated
	 */
	EAttribute getRelationToCourse_CreditsReduction();

	/**
	 * Returns the meta object for class '{@link coursePage.StudyProgram <em>Study Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study Program</em>'.
	 * @see coursePage.StudyProgram
	 * @generated
	 */
	EClass getStudyProgram();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.StudyProgram#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see coursePage.StudyProgram#getCode()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EAttribute getStudyProgram_Code();

	/**
	 * Returns the meta object for the reference '{@link coursePage.StudyProgram#getCanSignUpFor <em>Can Sign Up For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Can Sign Up For</em>'.
	 * @see coursePage.StudyProgram#getCanSignUpFor()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EReference getStudyProgram_CanSignUpFor();

	/**
	 * Returns the meta object for the container reference '{@link coursePage.StudyProgram#getBelongsTo <em>Belongs To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Belongs To</em>'.
	 * @see coursePage.StudyProgram#getBelongsTo()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EReference getStudyProgram_BelongsTo();

	/**
	 * Returns the meta object for class '{@link coursePage.Slot <em>Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Slot</em>'.
	 * @see coursePage.Slot
	 * @generated
	 */
	EClass getSlot();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Slot#getDay <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Day</em>'.
	 * @see coursePage.Slot#getDay()
	 * @see #getSlot()
	 * @generated
	 */
	EAttribute getSlot_Day();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Slot#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room</em>'.
	 * @see coursePage.Slot#getRoom()
	 * @see #getSlot()
	 * @generated
	 */
	EAttribute getSlot_Room();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Slot#getLectureType <em>Lecture Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lecture Type</em>'.
	 * @see coursePage.Slot#getLectureType()
	 * @see #getSlot()
	 * @generated
	 */
	EAttribute getSlot_LectureType();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Slot#getStarteTime <em>Starte Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Starte Time</em>'.
	 * @see coursePage.Slot#getStarteTime()
	 * @see #getSlot()
	 * @generated
	 */
	EAttribute getSlot_StarteTime();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Slot#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see coursePage.Slot#getEndTime()
	 * @see #getSlot()
	 * @generated
	 */
	EAttribute getSlot_EndTime();

	/**
	 * Returns the meta object for the reference list '{@link coursePage.Slot#getScheduleFor <em>Schedule For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Schedule For</em>'.
	 * @see coursePage.Slot#getScheduleFor()
	 * @see #getSlot()
	 * @generated
	 */
	EReference getSlot_ScheduleFor();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Slot#getHours <em>Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hours</em>'.
	 * @see coursePage.Slot#getHours()
	 * @see #getSlot()
	 * @generated
	 */
	EAttribute getSlot_Hours();

	/**
	 * Returns the meta object for class '{@link coursePage.Evaluation <em>Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluation</em>'.
	 * @see coursePage.Evaluation
	 * @generated
	 */
	EClass getEvaluation();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Evaluation#getEvaluation <em>Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Evaluation</em>'.
	 * @see coursePage.Evaluation#getEvaluation()
	 * @see #getEvaluation()
	 * @generated
	 */
	EAttribute getEvaluation_Evaluation();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Evaluation#getPoints <em>Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Points</em>'.
	 * @see coursePage.Evaluation#getPoints()
	 * @see #getEvaluation()
	 * @generated
	 */
	EAttribute getEvaluation_Points();

	/**
	 * Returns the meta object for class '{@link coursePage.CourseWork <em>Course Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Work</em>'.
	 * @see coursePage.CourseWork
	 * @generated
	 */
	EClass getCourseWork();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.CourseWork#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lecture Hours</em>'.
	 * @see coursePage.CourseWork#getLectureHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_LectureHours();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.CourseWork#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lab Hours</em>'.
	 * @see coursePage.CourseWork#getLabHours()
	 * @see #getCourseWork()
	 * @generated
	 */
	EAttribute getCourseWork_LabHours();

	/**
	 * Returns the meta object for class '{@link coursePage.CourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Instance</em>'.
	 * @see coursePage.CourseInstance
	 * @generated
	 */
	EClass getCourseInstance();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.CourseInstance#getSemester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester</em>'.
	 * @see coursePage.CourseInstance#getSemester()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Semester();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.CourseInstance#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see coursePage.CourseInstance#getCredits()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Credits();

	/**
	 * Returns the meta object for class '{@link coursePage.Lecturer <em>Lecturer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lecturer</em>'.
	 * @see coursePage.Lecturer
	 * @generated
	 */
	EClass getLecturer();

	/**
	 * Returns the meta object for the attribute '{@link coursePage.Lecturer#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Role</em>'.
	 * @see coursePage.Lecturer#getRole()
	 * @see #getLecturer()
	 * @generated
	 */
	EAttribute getLecturer_Role();

	/**
	 * Returns the meta object for class '{@link coursePage.CourseCordinator <em>Course Cordinator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Cordinator</em>'.
	 * @see coursePage.CourseCordinator
	 * @generated
	 */
	EClass getCourseCordinator();

	/**
	 * Returns the meta object for class '{@link coursePage.Staff <em>Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Staff</em>'.
	 * @see coursePage.Staff
	 * @generated
	 */
	EClass getStaff();

	/**
	 * Returns the meta object for the containment reference list '{@link coursePage.Staff#getInTheStaff <em>In The Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>In The Staff</em>'.
	 * @see coursePage.Staff#getInTheStaff()
	 * @see #getStaff()
	 * @generated
	 */
	EReference getStaff_InTheStaff();

	/**
	 * Returns the meta object for the container reference '{@link coursePage.Staff#getWorksFor <em>Works For</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Works For</em>'.
	 * @see coursePage.Staff#getWorksFor()
	 * @see #getStaff()
	 * @generated
	 */
	EReference getStaff_WorksFor();

	/**
	 * Returns the meta object for enum '{@link coursePage.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Role</em>'.
	 * @see coursePage.Role
	 * @generated
	 */
	EEnum getRole();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoursePageFactory getCoursePageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link coursePage.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.CourseImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CONTENT = eINSTANCE.getCourse_Content();

		/**
		 * The meta object literal for the '<em><b>Timetable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__TIMETABLE = eINSTANCE.getCourse_Timetable();

		/**
		 * The meta object literal for the '<em><b>Relation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__RELATION = eINSTANCE.getCourse_Relation();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS = eINSTANCE.getCourse_Credits();

		/**
		 * The meta object literal for the '<em><b>Grading</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__GRADING = eINSTANCE.getCourse_Grading();

		/**
		 * The meta object literal for the '<em><b>Required</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__REQUIRED = eINSTANCE.getCourse_Required();

		/**
		 * The meta object literal for the '<em><b>Course Work</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__COURSE_WORK = eINSTANCE.getCourse_CourseWork();

		/**
		 * The meta object literal for the '<em><b>Has Been Taught</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__HAS_BEEN_TAUGHT = eINSTANCE.getCourse_HasBeenTaught();

		/**
		 * The meta object literal for the '<em><b>Planned For</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__PLANNED_FOR = eINSTANCE.getCourse_PlannedFor();

		/**
		 * The meta object literal for the '<em><b>Semester</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__SEMESTER = eINSTANCE.getCourse_Semester();

		/**
		 * The meta object literal for the '<em><b>Course Staff</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__COURSE_STAFF = eINSTANCE.getCourse_CourseStaff();

		/**
		 * The meta object literal for the '<em><b>Belongs To</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__BELONGS_TO = eINSTANCE.getCourse_BelongsTo();

		/**
		 * The meta object literal for the '{@link coursePage.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.DepartmentImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__NAME = eINSTANCE.getDepartment_Name();

		/**
		 * The meta object literal for the '<em><b>Has</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__HAS = eINSTANCE.getDepartment_Has();

		/**
		 * The meta object literal for the '<em><b>Responsible Forr</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__RESPONSIBLE_FORR = eINSTANCE.getDepartment_ResponsibleForr();

		/**
		 * The meta object literal for the '<em><b>Responsible For</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__RESPONSIBLE_FOR = eINSTANCE.getDepartment_ResponsibleFor();

		/**
		 * The meta object literal for the '{@link coursePage.EvaluationForm <em>Evaluation Form</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.EvaluationForm
		 * @see coursePage.impl.CoursePagePackageImpl#getEvaluationForm()
		 * @generated
		 */
		EEnum EVALUATION_FORM = eINSTANCE.getEvaluationForm();

		/**
		 * The meta object literal for the '{@link coursePage.RelatedCourse <em>Related Course</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.RelatedCourse
		 * @see coursePage.impl.CoursePagePackageImpl#getRelatedCourse()
		 * @generated
		 */
		EEnum RELATED_COURSE = eINSTANCE.getRelatedCourse();

		/**
		 * The meta object literal for the '{@link coursePage.LectureType <em>Lecture Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.LectureType
		 * @see coursePage.impl.CoursePagePackageImpl#getLectureType()
		 * @generated
		 */
		EEnum LECTURE_TYPE = eINSTANCE.getLectureType();

		/**
		 * The meta object literal for the '{@link coursePage.Day <em>Day</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.Day
		 * @see coursePage.impl.CoursePagePackageImpl#getDay()
		 * @generated
		 */
		EEnum DAY = eINSTANCE.getDay();

		/**
		 * The meta object literal for the '{@link coursePage.Room <em>Room</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.Room
		 * @see coursePage.impl.CoursePagePackageImpl#getRoom()
		 * @generated
		 */
		EEnum ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '{@link coursePage.StudyCode <em>Study Code</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.StudyCode
		 * @see coursePage.impl.CoursePagePackageImpl#getStudyCode()
		 * @generated
		 */
		EEnum STUDY_CODE = eINSTANCE.getStudyCode();

		/**
		 * The meta object literal for the '{@link coursePage.impl.TimeTableImpl <em>Time Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.TimeTableImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getTimeTable()
		 * @generated
		 */
		EClass TIME_TABLE = eINSTANCE.getTimeTable();

		/**
		 * The meta object literal for the '<em><b>Table</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_TABLE__TABLE = eINSTANCE.getTimeTable_Table();

		/**
		 * The meta object literal for the '<em><b>For</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_TABLE__FOR = eINSTANCE.getTimeTable_For();

		/**
		 * The meta object literal for the '{@link coursePage.impl.EmployeeImpl <em>Employee</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.EmployeeImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getEmployee()
		 * @generated
		 */
		EClass EMPLOYEE = eINSTANCE.getEmployee();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMPLOYEE__NAME = eINSTANCE.getEmployee_Name();

		/**
		 * The meta object literal for the '<em><b>Works For</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMPLOYEE__WORKS_FOR = eINSTANCE.getEmployee_WorksFor();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMPLOYEE__ROLE = eINSTANCE.getEmployee_Role();

		/**
		 * The meta object literal for the '<em><b>Is CC</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMPLOYEE__IS_CC = eINSTANCE.getEmployee_IsCC();

		/**
		 * The meta object literal for the '<em><b>In This</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EMPLOYEE__IN_THIS = eINSTANCE.getEmployee_InThis();

		/**
		 * The meta object literal for the '{@link coursePage.impl.RelationToCourseImpl <em>Relation To Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.RelationToCourseImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getRelationToCourse()
		 * @generated
		 */
		EClass RELATION_TO_COURSE = eINSTANCE.getRelationToCourse();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATION_TO_COURSE__COURSE = eINSTANCE.getRelationToCourse_Course();

		/**
		 * The meta object literal for the '<em><b>Credits Reduction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATION_TO_COURSE__CREDITS_REDUCTION = eINSTANCE.getRelationToCourse_CreditsReduction();

		/**
		 * The meta object literal for the '{@link coursePage.impl.StudyProgramImpl <em>Study Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.StudyProgramImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getStudyProgram()
		 * @generated
		 */
		EClass STUDY_PROGRAM = eINSTANCE.getStudyProgram();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY_PROGRAM__CODE = eINSTANCE.getStudyProgram_Code();

		/**
		 * The meta object literal for the '<em><b>Can Sign Up For</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_PROGRAM__CAN_SIGN_UP_FOR = eINSTANCE.getStudyProgram_CanSignUpFor();

		/**
		 * The meta object literal for the '<em><b>Belongs To</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_PROGRAM__BELONGS_TO = eINSTANCE.getStudyProgram_BelongsTo();

		/**
		 * The meta object literal for the '{@link coursePage.impl.SlotImpl <em>Slot</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.SlotImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getSlot()
		 * @generated
		 */
		EClass SLOT = eINSTANCE.getSlot();

		/**
		 * The meta object literal for the '<em><b>Day</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLOT__DAY = eINSTANCE.getSlot_Day();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLOT__ROOM = eINSTANCE.getSlot_Room();

		/**
		 * The meta object literal for the '<em><b>Lecture Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLOT__LECTURE_TYPE = eINSTANCE.getSlot_LectureType();

		/**
		 * The meta object literal for the '<em><b>Starte Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLOT__STARTE_TIME = eINSTANCE.getSlot_StarteTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLOT__END_TIME = eINSTANCE.getSlot_EndTime();

		/**
		 * The meta object literal for the '<em><b>Schedule For</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SLOT__SCHEDULE_FOR = eINSTANCE.getSlot_ScheduleFor();

		/**
		 * The meta object literal for the '<em><b>Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLOT__HOURS = eINSTANCE.getSlot_Hours();

		/**
		 * The meta object literal for the '{@link coursePage.impl.EvaluationImpl <em>Evaluation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.EvaluationImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getEvaluation()
		 * @generated
		 */
		EClass EVALUATION = eINSTANCE.getEvaluation();

		/**
		 * The meta object literal for the '<em><b>Evaluation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION__EVALUATION = eINSTANCE.getEvaluation_Evaluation();

		/**
		 * The meta object literal for the '<em><b>Points</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION__POINTS = eINSTANCE.getEvaluation_Points();

		/**
		 * The meta object literal for the '{@link coursePage.impl.CourseWorkImpl <em>Course Work</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.CourseWorkImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getCourseWork()
		 * @generated
		 */
		EClass COURSE_WORK = eINSTANCE.getCourseWork();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__LECTURE_HOURS = eINSTANCE.getCourseWork_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_WORK__LAB_HOURS = eINSTANCE.getCourseWork_LabHours();

		/**
		 * The meta object literal for the '{@link coursePage.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.CourseInstanceImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getCourseInstance()
		 * @generated
		 */
		EClass COURSE_INSTANCE = eINSTANCE.getCourseInstance();

		/**
		 * The meta object literal for the '<em><b>Semester</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__SEMESTER = eINSTANCE.getCourseInstance_Semester();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__CREDITS = eINSTANCE.getCourseInstance_Credits();

		/**
		 * The meta object literal for the '{@link coursePage.impl.LecturerImpl <em>Lecturer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.LecturerImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getLecturer()
		 * @generated
		 */
		EClass LECTURER = eINSTANCE.getLecturer();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LECTURER__ROLE = eINSTANCE.getLecturer_Role();

		/**
		 * The meta object literal for the '{@link coursePage.impl.CourseCordinatorImpl <em>Course Cordinator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.CourseCordinatorImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getCourseCordinator()
		 * @generated
		 */
		EClass COURSE_CORDINATOR = eINSTANCE.getCourseCordinator();

		/**
		 * The meta object literal for the '{@link coursePage.impl.StaffImpl <em>Staff</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.impl.StaffImpl
		 * @see coursePage.impl.CoursePagePackageImpl#getStaff()
		 * @generated
		 */
		EClass STAFF = eINSTANCE.getStaff();

		/**
		 * The meta object literal for the '<em><b>In The Staff</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STAFF__IN_THE_STAFF = eINSTANCE.getStaff_InTheStaff();

		/**
		 * The meta object literal for the '<em><b>Works For</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STAFF__WORKS_FOR = eINSTANCE.getStaff_WorksFor();

		/**
		 * The meta object literal for the '{@link coursePage.Role <em>Role</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see coursePage.Role
		 * @see coursePage.impl.CoursePagePackageImpl#getRole()
		 * @generated
		 */
		EEnum ROLE = eINSTANCE.getRole();

	}

} //CoursePagePackage
