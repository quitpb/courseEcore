/**
 */
package coursePage;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Employee</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.Employee#getName <em>Name</em>}</li>
 *   <li>{@link coursePage.Employee#getWorksFor <em>Works For</em>}</li>
 *   <li>{@link coursePage.Employee#getRole <em>Role</em>}</li>
 *   <li>{@link coursePage.Employee#getIsCC <em>Is CC</em>}</li>
 *   <li>{@link coursePage.Employee#getInThis <em>In This</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getEmployee()
 * @model
 * @generated
 */
public interface Employee extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see coursePage.CoursePagePackage#getEmployee_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link coursePage.Employee#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Works For</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link coursePage.Department#getHas <em>Has</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Works For</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Works For</em>' container reference.
	 * @see #setWorksFor(Department)
	 * @see coursePage.CoursePagePackage#getEmployee_WorksFor()
	 * @see coursePage.Department#getHas
	 * @model opposite="has" transient="false"
	 * @generated
	 */
	Department getWorksFor();

	/**
	 * Sets the value of the '{@link coursePage.Employee#getWorksFor <em>Works For</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Works For</em>' container reference.
	 * @see #getWorksFor()
	 * @generated
	 */
	void setWorksFor(Department value);

	/**
	 * Returns the value of the '<em><b>Role</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.Lecturer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getEmployee_Role()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Lecturer> getRole();

	/**
	 * Returns the value of the '<em><b>Is CC</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.CourseCordinator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is CC</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is CC</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getEmployee_IsCC()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<CourseCordinator> getIsCC();

	/**
	 * Returns the value of the '<em><b>In This</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link coursePage.Staff#getInTheStaff <em>In The Staff</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In This</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In This</em>' container reference.
	 * @see #setInThis(Staff)
	 * @see coursePage.CoursePagePackage#getEmployee_InThis()
	 * @see coursePage.Staff#getInTheStaff
	 * @model opposite="inTheStaff" transient="false"
	 * @generated
	 */
	Staff getInThis();

	/**
	 * Sets the value of the '{@link coursePage.Employee#getInThis <em>In This</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In This</em>' container reference.
	 * @see #getInThis()
	 * @generated
	 */
	void setInThis(Staff value);

} // Employee
