/**
 */
package coursePage;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lecture</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.Lecture#getLectureTime <em>Lecture Time</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getLecture()
 * @model
 * @generated
 */
public interface Lecture extends EObject {

	/**
	 * Returns the value of the '<em><b>Lecture Time</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecture Time</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecture Time</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getLecture_LectureTime()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Event> getLectureTime();
} // Lecture
