/**
 */
package coursePage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link coursePage.Department#getName <em>Name</em>}</li>
 *   <li>{@link coursePage.Department#getHas <em>Has</em>}</li>
 *   <li>{@link coursePage.Department#getResponsibleForr <em>Responsible Forr</em>}</li>
 *   <li>{@link coursePage.Department#getResponsibleFor <em>Responsible For</em>}</li>
 * </ul>
 *
 * @see coursePage.CoursePagePackage#getDepartment()
 * @model
 * @generated
 */
public interface Department extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see coursePage.CoursePagePackage#getDepartment_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link coursePage.Department#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Has</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.Employee}.
	 * It is bidirectional and its opposite is '{@link coursePage.Employee#getWorksFor <em>Works For</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getDepartment_Has()
	 * @see coursePage.Employee#getWorksFor
	 * @model opposite="WorksFor" containment="true" required="true"
	 * @generated
	 */
	EList<Employee> getHas();

	/**
	 * Returns the value of the '<em><b>Responsible Forr</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.Course}.
	 * It is bidirectional and its opposite is '{@link coursePage.Course#getBelongsTo <em>Belongs To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible Forr</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible Forr</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getDepartment_ResponsibleForr()
	 * @see coursePage.Course#getBelongsTo
	 * @model opposite="belongsTo" containment="true" required="true"
	 * @generated
	 */
	EList<Course> getResponsibleForr();

	/**
	 * Returns the value of the '<em><b>Responsible For</b></em>' containment reference list.
	 * The list contents are of type {@link coursePage.StudyProgram}.
	 * It is bidirectional and its opposite is '{@link coursePage.StudyProgram#getBelongsTo <em>Belongs To</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible For</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible For</em>' containment reference list.
	 * @see coursePage.CoursePagePackage#getDepartment_ResponsibleFor()
	 * @see coursePage.StudyProgram#getBelongsTo
	 * @model opposite="belongsTo" containment="true"
	 * @generated
	 */
	EList<StudyProgram> getResponsibleFor();

} // Department
