package tdt4250.transformation

import org.w3c.xhtml1.util.Xhtml1ResourceFactoryImpl
import org.w3c.xhtml1.Xhtml1Factory
import coursePage.Course
import org.w3c.xhtml1.HtmlType
import org.eclipse.emf.ecore.xmi.XMLResource
import java.io.ByteArrayOutputStream
import org.eclipse.emf.common.util.URI
import java.io.IOException
import java.util.Arrays
import org.eclipse.emf.ecore.xml.namespace.XMLNamespaceFactory
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import coursePage.CoursePagePackage
import org.eclipse.emf.ecore.EObject
import coursePage.util.CoursePageResourceImpl
import coursePage.util.CoursePageResourceFactoryImpl

class Course2HTML {
	
	Xhtml1ResourceFactoryImpl xhtml1ResourceFactoryImpl = new Xhtml1ResourceFactoryImpl
	
		def String generateHtml(Course course) {
		val encoding = "UTF-8"
		val html = generateHtmlType(course);
		val root = XMLNamespaceFactory.eINSTANCE.createXMLNamespaceDocumentRoot += html
		//val fileName = (if (course.name !== null) course.name.replace('.', '/') else "quiz")+ '.html'
		val fileName = "Course" + '.html'
		
		val resource = xhtml1ResourceFactoryImpl.createResource(URI.createFileURI(fileName)) as XMLResource
		resource.getDefaultSaveOptions().put(XMLResource.OPTION_ENCODING, encoding);
		resource.contents += html
		val outputStream = new ByteArrayOutputStream(4096)
		resource.save(outputStream, null)
		val originalOutput = outputStream.toString(encoding)
		originalOutput.cleanHtml
	}
	
		def cleanHtml(String html) {	
		html
			.replace("xhtml:", "")
			.replace("html_._type", "html")
			.replace("xmlns:xhtml=", "xmlns=")
			.replace("&lt;", "<")
			.replace("&gt;", ">")
			.replace("&amp;", "&")
			.replace("&quot;", "'")
	}
	
	extension Xhtml1Factory xhtml1Factory = Xhtml1Factory.eINSTANCE
	extension XhtmlUtil xhtmlUtil = new XhtmlUtil
	
	def HtmlType generateHtmlType(Course course) {
		val html = createHtmlType => [
			head = createHeadType => [
				title = createTitleType => [
					it += ("TITLE")
				]
				meta += createMetaType => [
					httpEquiv = "content-type"
					content = "text/html; charset=UTF-8"
				]
			]
			body = createBodyType => [
				it +=createH1Type => [
					it += course.code + " " + course.name
				]
				
				it += createPType => [
					it += "This page contains all information about this course" 
				]
				
				it += createPType => [
					it += "name: " + course.code + " " + course.name
				]

				it += createPType => [
					it += "credits: " + course.credits 
				]

				it += createH2Type => [
					it += "TimeTable"
				]
				for(slot: course.timetable.table){
					it += createPType =>[
						it += slot.day.toString + " " + slot.room.toString + " " +slot.starteTime + "-" +slot.endTime
						+ " " + slot.lectureType
					]
				}

				
				it += createH2Type => [
					it += "Evaluation"
				]
				it += createPType => [
					for(evaluation : course.grading){
						it += evaluation.evaluation.toString
						+ " " + evaluation.points+ "/" + "100"
						
					} 
				]
				
				it += createH2Type => [
					it += "Course Work"
				]
				
				it += createPType => [
					it  += "Lecture Hours" + " " + course.courseWork.lectureHours.toString
				]

				
				it += createPType => [
					it  += "Lab Hours" + " " + course.courseWork.labHours.toString
				]
				
				it += createH2Type => [
					it += "Course Reduction"
				]
				
				for(ocourse :course.relation ){
					it += createPType =>[
						it+= ocourse.course.toString + " " + "Reduction:" + ocourse.creditsReduction.toString
					]
				}
				it += createH2Type => [
					it += "Course Staff"
				]
				
				for(person: course.courseStaff.get(0).inTheStaff){
					it += createPType =>[
						it += person.name + " " + person.role
					]
				}
				
				
			]
		]
		html
//		fsa.generateFile(quiz.name.replace('.', '/') + '.html', stringBuilder);
	}
	
		def static void main(String[] args) throws IOException {
		val argsAsList = Arrays.asList(args)
		val course = if (argsAsList.size > 0) getCourse(argsAsList.get(0)) else getSampleCourse();
		val html = new Course2HTML().generateHtml(course);
		System.out.println(html);
		}
		
		def static Course getSampleCourse() {
			try {
				return getCourse(Course2HTML.getResource("Course.xmi").toString());
				} 
			catch (IOException e) {
				println();
				System.err.println(e);
				return null;
		}
	}
	
	def static Course getCourse(String uriString) throws IOException {
		val resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(CoursePagePackage.eNS_URI, CoursePagePackage.eINSTANCE);
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new CoursePageResourceFactoryImpl());
		val resource = resSet.getResource(URI.createURI(uriString), true);
		for (EObject eObject : resource.getContents()) {
			if (eObject instanceof Course) {
				return eObject as Course;
			}
		}
		return null;
	}
		
		
	
	

	

}