#Transformation

##Plugin and extensions
Plug-in added to Plug-In dependencies: **org.w3c.xhtml1**
Extension used in Course2HTML.xtend: **Xhtml1Factory** and **XhtmlUtil**

**Xhtml1Factory**
With this extension we can implement methods to generate html tags/types like createH1Type and createH2Type

**XhtmlUtil**
Utilize methods from Xhtml1Factory. 


##Transformation

In this model i have done a Model 2 Text transformation.

Source model : xmi
Target model: Text in html format

To run the the sample provided in the transformation
1. Open Course2HTML.xtend in /tdt4250.web/src/tdt4250/transformation/Course2HTML.xtend
2. Right Click,  Run As > Java Application
3. Console will display the output of the transfomration
