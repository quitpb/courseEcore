package tdt4250.transformation;

import coursePage.Course;
import coursePage.CoursePagePackage;
import coursePage.Employee;
import coursePage.Evaluation;
import coursePage.LectureType;
import coursePage.Lecturer;
import coursePage.RelationToCourse;
import coursePage.Slot;
import coursePage.util.CoursePageResourceFactoryImpl;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xml.namespace.XMLNamespaceDocumentRoot;
import org.eclipse.emf.ecore.xml.namespace.XMLNamespaceFactory;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.w3c.xhtml1.BodyType;
import org.w3c.xhtml1.H1Type;
import org.w3c.xhtml1.H2Type;
import org.w3c.xhtml1.HeadType;
import org.w3c.xhtml1.HtmlType;
import org.w3c.xhtml1.MetaType;
import org.w3c.xhtml1.PType;
import org.w3c.xhtml1.TitleType;
import org.w3c.xhtml1.Xhtml1Factory;
import org.w3c.xhtml1.util.Xhtml1ResourceFactoryImpl;
import tdt4250.transformation.XhtmlUtil;

@SuppressWarnings("all")
public class Course2HTML {
  private Xhtml1ResourceFactoryImpl xhtml1ResourceFactoryImpl = new Xhtml1ResourceFactoryImpl();
  
  public String generateHtml(final Course course) {
    try {
      String _xblockexpression = null;
      {
        final String encoding = "UTF-8";
        final HtmlType html = this.generateHtmlType(course);
        XMLNamespaceDocumentRoot _createXMLNamespaceDocumentRoot = XMLNamespaceFactory.eINSTANCE.createXMLNamespaceDocumentRoot();
        final EObject root = this.xhtmlUtil.operator_add(_createXMLNamespaceDocumentRoot, html);
        final String fileName = ("Course" + ".html");
        Resource _createResource = this.xhtml1ResourceFactoryImpl.createResource(URI.createFileURI(fileName));
        final XMLResource resource = ((XMLResource) _createResource);
        resource.getDefaultSaveOptions().put(XMLResource.OPTION_ENCODING, encoding);
        EList<EObject> _contents = resource.getContents();
        _contents.add(html);
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(4096);
        resource.save(outputStream, null);
        final String originalOutput = outputStream.toString(encoding);
        _xblockexpression = this.cleanHtml(originalOutput);
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public String cleanHtml(final String html) {
    return html.replace("xhtml:", "").replace("html_._type", "html").replace("xmlns:xhtml=", "xmlns=").replace("&lt;", "<").replace("&gt;", ">").replace("&amp;", "&").replace("&quot;", "\'");
  }
  
  @Extension
  private Xhtml1Factory xhtml1Factory = Xhtml1Factory.eINSTANCE;
  
  @Extension
  private XhtmlUtil xhtmlUtil = new XhtmlUtil();
  
  public HtmlType generateHtmlType(final Course course) {
    HtmlType _xblockexpression = null;
    {
      HtmlType _createHtmlType = this.xhtml1Factory.createHtmlType();
      final Procedure1<HtmlType> _function = (HtmlType it) -> {
        HeadType _createHeadType = this.xhtml1Factory.createHeadType();
        final Procedure1<HeadType> _function_1 = (HeadType it_1) -> {
          TitleType _createTitleType = this.xhtml1Factory.createTitleType();
          final Procedure1<TitleType> _function_2 = (TitleType it_2) -> {
            this.xhtmlUtil.operator_add(it_2, "TITLE");
          };
          TitleType _doubleArrow = ObjectExtensions.<TitleType>operator_doubleArrow(_createTitleType, _function_2);
          it_1.setTitle(_doubleArrow);
          EList<MetaType> _meta = it_1.getMeta();
          MetaType _createMetaType = this.xhtml1Factory.createMetaType();
          final Procedure1<MetaType> _function_3 = (MetaType it_2) -> {
            it_2.setHttpEquiv("content-type");
            it_2.setContent("text/html; charset=UTF-8");
          };
          MetaType _doubleArrow_1 = ObjectExtensions.<MetaType>operator_doubleArrow(_createMetaType, _function_3);
          _meta.add(_doubleArrow_1);
        };
        HeadType _doubleArrow = ObjectExtensions.<HeadType>operator_doubleArrow(_createHeadType, _function_1);
        it.setHead(_doubleArrow);
        BodyType _createBodyType = this.xhtml1Factory.createBodyType();
        final Procedure1<BodyType> _function_2 = (BodyType it_1) -> {
          H1Type _createH1Type = this.xhtml1Factory.createH1Type();
          final Procedure1<H1Type> _function_3 = (H1Type it_2) -> {
            String _code = course.getCode();
            String _plus = (_code + " ");
            String _name = course.getName();
            String _plus_1 = (_plus + _name);
            this.xhtmlUtil.operator_add(it_2, _plus_1);
          };
          H1Type _doubleArrow_1 = ObjectExtensions.<H1Type>operator_doubleArrow(_createH1Type, _function_3);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_1);
          PType _createPType = this.xhtml1Factory.createPType();
          final Procedure1<PType> _function_4 = (PType it_2) -> {
            this.xhtmlUtil.operator_add(it_2, "This page contains all information about this course");
          };
          PType _doubleArrow_2 = ObjectExtensions.<PType>operator_doubleArrow(_createPType, _function_4);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_2);
          PType _createPType_1 = this.xhtml1Factory.createPType();
          final Procedure1<PType> _function_5 = (PType it_2) -> {
            String _code = course.getCode();
            String _plus = ("name: " + _code);
            String _plus_1 = (_plus + " ");
            String _name = course.getName();
            String _plus_2 = (_plus_1 + _name);
            this.xhtmlUtil.operator_add(it_2, _plus_2);
          };
          PType _doubleArrow_3 = ObjectExtensions.<PType>operator_doubleArrow(_createPType_1, _function_5);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_3);
          PType _createPType_2 = this.xhtml1Factory.createPType();
          final Procedure1<PType> _function_6 = (PType it_2) -> {
            double _credits = course.getCredits();
            String _plus = ("credits: " + Double.valueOf(_credits));
            this.xhtmlUtil.operator_add(it_2, _plus);
          };
          PType _doubleArrow_4 = ObjectExtensions.<PType>operator_doubleArrow(_createPType_2, _function_6);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_4);
          H2Type _createH2Type = this.xhtml1Factory.createH2Type();
          final Procedure1<H2Type> _function_7 = (H2Type it_2) -> {
            this.xhtmlUtil.operator_add(it_2, "TimeTable");
          };
          H2Type _doubleArrow_5 = ObjectExtensions.<H2Type>operator_doubleArrow(_createH2Type, _function_7);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_5);
          EList<Slot> _table = course.getTimetable().getTable();
          for (final Slot slot : _table) {
            PType _createPType_3 = this.xhtml1Factory.createPType();
            final Procedure1<PType> _function_8 = (PType it_2) -> {
              String _string = slot.getDay().toString();
              String _plus = (_string + " ");
              String _string_1 = slot.getRoom().toString();
              String _plus_1 = (_plus + _string_1);
              String _plus_2 = (_plus_1 + " ");
              String _starteTime = slot.getStarteTime();
              String _plus_3 = (_plus_2 + _starteTime);
              String _plus_4 = (_plus_3 + "-");
              String _endTime = slot.getEndTime();
              String _plus_5 = (_plus_4 + _endTime);
              String _plus_6 = (_plus_5 + " ");
              LectureType _lectureType = slot.getLectureType();
              String _plus_7 = (_plus_6 + _lectureType);
              this.xhtmlUtil.operator_add(it_2, _plus_7);
            };
            PType _doubleArrow_6 = ObjectExtensions.<PType>operator_doubleArrow(_createPType_3, _function_8);
            this.xhtmlUtil.operator_add(it_1, _doubleArrow_6);
          }
          H2Type _createH2Type_1 = this.xhtml1Factory.createH2Type();
          final Procedure1<H2Type> _function_9 = (H2Type it_2) -> {
            this.xhtmlUtil.operator_add(it_2, "Evaluation");
          };
          H2Type _doubleArrow_7 = ObjectExtensions.<H2Type>operator_doubleArrow(_createH2Type_1, _function_9);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_7);
          PType _createPType_4 = this.xhtml1Factory.createPType();
          final Procedure1<PType> _function_10 = (PType it_2) -> {
            EList<Evaluation> _grading = course.getGrading();
            for (final Evaluation evaluation : _grading) {
              String _string = evaluation.getEvaluation().toString();
              String _plus = (_string + " ");
              double _points = evaluation.getPoints();
              String _plus_1 = (_plus + Double.valueOf(_points));
              String _plus_2 = (_plus_1 + "/");
              String _plus_3 = (_plus_2 + "100");
              this.xhtmlUtil.operator_add(it_2, _plus_3);
            }
          };
          PType _doubleArrow_8 = ObjectExtensions.<PType>operator_doubleArrow(_createPType_4, _function_10);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_8);
          H2Type _createH2Type_2 = this.xhtml1Factory.createH2Type();
          final Procedure1<H2Type> _function_11 = (H2Type it_2) -> {
            this.xhtmlUtil.operator_add(it_2, "Course Work");
          };
          H2Type _doubleArrow_9 = ObjectExtensions.<H2Type>operator_doubleArrow(_createH2Type_2, _function_11);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_9);
          PType _createPType_5 = this.xhtml1Factory.createPType();
          final Procedure1<PType> _function_12 = (PType it_2) -> {
            String _string = Double.valueOf(course.getCourseWork().getLectureHours()).toString();
            String _plus = (("Lecture Hours" + " ") + _string);
            this.xhtmlUtil.operator_add(it_2, _plus);
          };
          PType _doubleArrow_10 = ObjectExtensions.<PType>operator_doubleArrow(_createPType_5, _function_12);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_10);
          PType _createPType_6 = this.xhtml1Factory.createPType();
          final Procedure1<PType> _function_13 = (PType it_2) -> {
            String _string = Double.valueOf(course.getCourseWork().getLabHours()).toString();
            String _plus = (("Lab Hours" + " ") + _string);
            this.xhtmlUtil.operator_add(it_2, _plus);
          };
          PType _doubleArrow_11 = ObjectExtensions.<PType>operator_doubleArrow(_createPType_6, _function_13);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_11);
          H2Type _createH2Type_3 = this.xhtml1Factory.createH2Type();
          final Procedure1<H2Type> _function_14 = (H2Type it_2) -> {
            this.xhtmlUtil.operator_add(it_2, "Course Reduction");
          };
          H2Type _doubleArrow_12 = ObjectExtensions.<H2Type>operator_doubleArrow(_createH2Type_3, _function_14);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_12);
          EList<RelationToCourse> _relation = course.getRelation();
          for (final RelationToCourse ocourse : _relation) {
            PType _createPType_7 = this.xhtml1Factory.createPType();
            final Procedure1<PType> _function_15 = (PType it_2) -> {
              String _string = ocourse.getCourse().toString();
              String _plus = (_string + " ");
              String _plus_1 = (_plus + "Reduction:");
              String _string_1 = Double.valueOf(ocourse.getCreditsReduction()).toString();
              String _plus_2 = (_plus_1 + _string_1);
              this.xhtmlUtil.operator_add(it_2, _plus_2);
            };
            PType _doubleArrow_13 = ObjectExtensions.<PType>operator_doubleArrow(_createPType_7, _function_15);
            this.xhtmlUtil.operator_add(it_1, _doubleArrow_13);
          }
          H2Type _createH2Type_4 = this.xhtml1Factory.createH2Type();
          final Procedure1<H2Type> _function_16 = (H2Type it_2) -> {
            this.xhtmlUtil.operator_add(it_2, "Course Staff");
          };
          H2Type _doubleArrow_14 = ObjectExtensions.<H2Type>operator_doubleArrow(_createH2Type_4, _function_16);
          this.xhtmlUtil.operator_add(it_1, _doubleArrow_14);
          EList<Employee> _inTheStaff = course.getCourseStaff().get(0).getInTheStaff();
          for (final Employee person : _inTheStaff) {
            PType _createPType_8 = this.xhtml1Factory.createPType();
            final Procedure1<PType> _function_17 = (PType it_2) -> {
              String _name = person.getName();
              String _plus = (_name + " ");
              EList<Lecturer> _role = person.getRole();
              String _plus_1 = (_plus + _role);
              this.xhtmlUtil.operator_add(it_2, _plus_1);
            };
            PType _doubleArrow_15 = ObjectExtensions.<PType>operator_doubleArrow(_createPType_8, _function_17);
            this.xhtmlUtil.operator_add(it_1, _doubleArrow_15);
          }
        };
        BodyType _doubleArrow_1 = ObjectExtensions.<BodyType>operator_doubleArrow(_createBodyType, _function_2);
        it.setBody(_doubleArrow_1);
      };
      final HtmlType html = ObjectExtensions.<HtmlType>operator_doubleArrow(_createHtmlType, _function);
      _xblockexpression = html;
    }
    return _xblockexpression;
  }
  
  public static void main(final String[] args) throws IOException {
    final List<String> argsAsList = Arrays.<String>asList(args);
    Course _xifexpression = null;
    int _size = argsAsList.size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      _xifexpression = Course2HTML.getCourse(argsAsList.get(0));
    } else {
      _xifexpression = Course2HTML.getSampleCourse();
    }
    final Course course = _xifexpression;
    final String html = new Course2HTML().generateHtml(course);
    System.out.println(html);
  }
  
  public static Course getSampleCourse() {
    try {
      return Course2HTML.getCourse(Course2HTML.class.getResource("Course.xmi").toString());
    } catch (final Throwable _t) {
      if (_t instanceof IOException) {
        final IOException e = (IOException)_t;
        InputOutput.println();
        System.err.println(e);
        return null;
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  public static Course getCourse(final String uriString) throws IOException {
    final ResourceSetImpl resSet = new ResourceSetImpl();
    resSet.getPackageRegistry().put(CoursePagePackage.eNS_URI, CoursePagePackage.eINSTANCE);
    Map<String, Object> _extensionToFactoryMap = resSet.getResourceFactoryRegistry().getExtensionToFactoryMap();
    CoursePageResourceFactoryImpl _coursePageResourceFactoryImpl = new CoursePageResourceFactoryImpl();
    _extensionToFactoryMap.put("xmi", _coursePageResourceFactoryImpl);
    final Resource resource = resSet.getResource(URI.createURI(uriString), true);
    EList<EObject> _contents = resource.getContents();
    for (final EObject eObject : _contents) {
      if ((eObject instanceof Course)) {
        return ((Course) eObject);
      }
    }
    return null;
  }
}
